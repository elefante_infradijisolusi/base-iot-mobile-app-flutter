import 'package:flutter/material.dart';

class NodeItem extends StatelessWidget {
  final int index;
  final String name;
  final String category;
  final dynamic status;
  final Function clickHandler;
  NodeItem(
      {this.index, this.name, this.category, this.status, this.clickHandler});

  Color findColor(String message) {
    if (message.toLowerCase().contains("heat") ||
        message.toLowerCase().contains("fuse")) {
      return Colors.red;
    } else if (message.toLowerCase().contains("balance") ||
        message.toLowerCase().contains("value")) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  Icon selecMarker(code) {
    if (code > 2) {
      return Icon(
        Icons.notifications_active,
        color: Colors.red,
        size: 30,
      );
    } else if (code <= 2 && code > 0) {
      return Icon(
        Icons.warning,
        color: Colors.orange,
        size: 30,
      );
    } else {
      return Icon(
        Icons.check_circle,
        color: Colors.green,
        size: 30,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(0.0),
        child: ListTile(
          visualDensity: VisualDensity(horizontal: 4.0),
          onTap: clickHandler,
          leading: Container(
            padding: EdgeInsets.only(right: 5.0),
            decoration: new BoxDecoration(
              border: new Border(
                right: new BorderSide(
                  width: 1.0,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
            child: selecMarker(status["code"]),
          ),
          subtitle: Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 0.0,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: status["message"]
                  .reversed
                  .toList()
                  .map<Widget>((item) => Center(
                        child: Container(
                          padding: EdgeInsets.all(3.0),
                          margin: EdgeInsets.only(right: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: findColor(item),
                              width: 1.0,
                            ),
                          ),
                          child: Center(
                              child: Text(
                            item,
                            style: TextStyle(
                                fontSize: 10,
                                color: findColor(item),
                                fontWeight: FontWeight.w400),
                          )),
                        ),
                      ))
                  .toList(),
            ),
          ),
          title: Container(
            child: Text(
              name.toUpperCase(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
