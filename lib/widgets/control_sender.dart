import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

import '../providers/controls.dart';
import '../models/control.dart';

class changeValueControl extends StatefulWidget {
  final String id;
  final Function sendHandler;

  const changeValueControl(this.id, this.sendHandler);

  @override
  _changeValueControlState createState() => _changeValueControlState();
}

class _changeValueControlState extends State<changeValueControl> {
  @override
  Widget build(BuildContext context) {
    var _initValues =
        Provider.of<Controls>(context, listen: false).findById(widget.id);

    Widget createInput(ControlType type) {
      if (type == ControlType.onof) {
        return Expanded(
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.height * 0.15,
              height: MediaQuery.of(context).size.height * 0.15,
              child: MaterialButton(
                elevation: 3,
                focusElevation: 0,
                splashColor: Colors.white,
                shape: CircleBorder(
                    side: BorderSide(
                        width: 2,
                        color: Colors.black,
                        style: BorderStyle.solid)),
                child: Text(
                  _initValues.value.toInt() == 1 ? "ON" : "OFF",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                color:
                    _initValues.value.toInt() == 1 ? Colors.green : Colors.red,
                textColor: Colors.white,
                onPressed: () {
                  setState(() {
                    Provider.of<Controls>(context, listen: false).updateProduct(
                        _initValues.id,
                        Control(
                          id: _initValues.id,
                          name: _initValues.name,
                          type: _initValues.type,
                          min: _initValues.min,
                          max: _initValues.max,
                          step: _initValues.step,
                          ip: _initValues.ip,
                          port: _initValues.port,
                          topic: _initValues.topic,
                          value:
                              ((_initValues.value.toInt() + 1) % 2).toDouble(),
                        ));

                    widget.sendHandler(_initValues.id);
                  });
                },
              ),
            ),
          ),
        );
      }
      if (type == ControlType.slider) {
        return Expanded(
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  flex: 1,
                  child: Text(
                    _initValues.value.round().toString(),
                    overflow: TextOverflow.fade,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Theme.of(context).accentColor, fontSize: 20),
                  ),
                ),
                Expanded(
                  flex: 8,
                  child: Slider(
                    value: _initValues.value.toDouble(),
                    min: _initValues.min,
                    max: _initValues.max,
                    divisions:
                        ((_initValues.max - _initValues.min) / _initValues.step)
                            .toInt(),
                    activeColor: Theme.of(context).accentColor,
                    inactiveColor: Colors.grey,
                    label: 'val : ${_initValues.value.round().toString()}',
                    onChanged: (double newValue) {
                      setState(() {
                        Provider.of<Controls>(context, listen: false)
                            .updateProduct(
                          _initValues.id,
                          Control(
                            id: _initValues.id,
                            name: _initValues.name,
                            type: _initValues.type,
                            min: _initValues.min,
                            max: _initValues.max,
                            step: _initValues.step,
                            ip: _initValues.ip,
                            port: _initValues.port,
                            topic: _initValues.topic,
                            value: newValue.round().toDouble(),
                          ),
                        );

                        widget.sendHandler(_initValues.id);
                      });
                    },
                    semanticFormatterCallback: (double newValue) {
                      return '${newValue.round()} dollars';
                    },
                  ),
                ),
                Container(
                  child: IconButton(
                    onPressed: () {
                      widget.sendHandler(_initValues.id);
                    },
                    icon: Icon(Icons.check),
                    color: Theme.of(context).accentColor,
                    iconSize: 35,
                  ),
                ),
              ],
            ),
          ),
        );
      }
      if (type == ControlType.value) {
        return Expanded(
          child: Center(
            child: Row(
              children: [
                Expanded(
                    child: NumberInputWithIncrementDecrement(
                  id: _initValues.id,
                  sendHandler: widget.sendHandler,
                )),
                Container(
                  child: IconButton(
                    onPressed: () {
                      widget.sendHandler(_initValues.id);
                    },
                    icon: Icon(Icons.check),
                    color: Theme.of(context).accentColor,
                    iconSize: 35,
                  ),
                ),
              ],
            ),
          ),
        );
      }
    }

    return SingleChildScrollView(
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(
              20.0, 20.0, 20.0, 20.0), // content padding
          child: Container(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              children: [
                Text(
                  _initValues.name,
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                createInput(_initValues.type)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class NumberInputWithIncrementDecrement extends StatefulWidget {
  final String id;
  final Function sendHandler;

  const NumberInputWithIncrementDecrement({this.id, this.sendHandler});
  @override
  _NumberInputWithIncrementDecrementState createState() =>
      _NumberInputWithIncrementDecrementState();
}

class _NumberInputWithIncrementDecrementState
    extends State<NumberInputWithIncrementDecrement> {
  TextEditingController _controller = TextEditingController();

  // @override
  // void initState() {
  //   super.initState();
  //   _controller.text =
  //       widget.value.toString(); // Setting the initial value for the field.
  // }

  @override
  Widget build(BuildContext context) {
    var _initValues =
        Provider.of<Controls>(context, listen: false).findById(widget.id);

    setState(() {
      _controller.text = _initValues.value.toString();
    });
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Center(
        child: Container(
          foregroundDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(
              color: Colors.blueGrey,
              width: 2.0,
            ),
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: TextFormField(
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(8.0),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  controller: _controller,
                  keyboardType: TextInputType.numberWithOptions(
                    decimal: false,
                    signed: true,
                  ),
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly
                  ],
                ),
              ),
              Container(
                height: 38.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 40,
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            width: 0.5,
                          ),
                        ),
                      ),
                      child: InkWell(
                        child: Icon(
                          Icons.arrow_drop_up,
                          size: 18.0,
                          color: Theme.of(context).accentColor,
                        ),
                        onTap: () {
                          double currentValue = double.parse(_controller.text);
                          setState(() {
                            currentValue = currentValue + _initValues.step;
                            if (currentValue < _initValues.max) {
                              _controller.text = (currentValue).toString();

                              Provider.of<Controls>(context, listen: false)
                                  .updateProduct(
                                      _initValues.id,
                                      Control(
                                        id: _initValues.id,
                                        name: _initValues.name,
                                        type: _initValues.type,
                                        min: _initValues.min,
                                        max: _initValues.max,
                                        step: _initValues.step,
                                        ip: _initValues.ip,
                                        port: _initValues.port,
                                        topic: _initValues.topic,
                                        value: currentValue,
                                      ));

                              widget.sendHandler(_initValues.id);
                            }
                            // incrementing value
                          });
                        },
                      ),
                    ),
                    InkWell(
                      child: Icon(
                        Icons.arrow_drop_down,
                        size: 18.0,
                        color: Theme.of(context).accentColor,
                      ),
                      onTap: () {
                        double currentValue = double.parse(_controller.text);

                        setState(() {
                          currentValue = currentValue - _initValues.step;
                          if (currentValue > _initValues.min) {
                            _controller.text =
                                (currentValue).toString(); // incrementing value

                            Provider.of<Controls>(context, listen: false)
                                .updateProduct(
                                    _initValues.id,
                                    Control(
                                      id: _initValues.id,
                                      name: _initValues.name,
                                      type: _initValues.type,
                                      min: _initValues.min,
                                      max: _initValues.max,
                                      step: _initValues.step,
                                      ip: _initValues.ip,
                                      port: _initValues.port,
                                      topic: _initValues.topic,
                                      value: currentValue,
                                    ));
                            widget.sendHandler(_initValues.id);
                          }
                        });
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
