import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';

class ButtonTab extends StatelessWidget {
  final Function changePageHandler;

  ButtonTab(this.changePageHandler);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        onTap: changePageHandler,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.red,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.white,

        // backgroundColor: Theme.of(context).primaryColor,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.map,
              // color: Theme.of(context).primaryColor,
            ),
            title: Text(
              "Maps",
              // style: TextStyle(
              //   color: Theme.of(context).primaryColor,
              // ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.insert_chart,
              // color: Theme.of(context).primaryColor,
            ),
            title: Text(
              "Chart",
              // style: TextStyle(
              //   color: Theme.of(context).primaryColor,
              // ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.touch_app,
              // color: Theme.of(context).primaryColor,
            ),
            title: Text(
              "Control",
              // style: TextStyle(
              //   color: Theme.of(context).primaryColor,
              // ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_circle,
              // color: Theme.of(context).primaryColor,
            ),
            title: Text(
              "User",
              // style: TextStyle(
              //   color: Theme.of(context).primaryColor,
              // ),
            ),
          ),
        ]
    );
  }
}