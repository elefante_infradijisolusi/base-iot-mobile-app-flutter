import 'dart:math';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/line_chart.dart';
import 'package:mp_chart/mp/controller/line_chart_controller.dart';
import 'package:mp_chart/mp/core/data/line_data.dart';
import 'package:mp_chart/mp/core/data_set/line_data_set.dart';
import 'package:mp_chart/mp/core/description.dart';
import 'package:mp_chart/mp/core/entry/entry.dart';
import 'package:mp_chart/mp/core/enums/mode.dart';
import 'package:mp_chart/mp/core/enums/y_axis_label_position.dart';
import 'package:http/http.dart' as http;

import '../helper/util.dart';
import '../helper/custom_theme.dart';

class LineChartView extends StatefulWidget {
  final String name;
  final String id;
  final List<dynamic> data_chart;

  const LineChartView({this.name, this.id, this.data_chart});

  @override
  _LineChartViewState createState() => _LineChartViewState();
}

class _LineChartViewState extends State<LineChartView> {
  LineChartController controller;
  @override
  void initState() {
    _initController();
    super.initState();
  }

  void _initController() {
    var desc = Description()..enabled = false;
    controller = LineChartController(
        axisLeftSettingFunction: (axisLeft, controller) {
          axisLeft
            ..typeface = Util.REGULAR
            ..setLabelCount2(6, false)
            ..textColor = (FintnessAppTheme.grey.withOpacity(0.5))
            ..textSize = 10
            ..position = (YAxisLabelPosition.INSIDE_CHART)
            ..drawGridLines = (false)
            ..axisLineColor = (Colors.transparent);
        },
        axisRightSettingFunction: (axisRight, controller) {
          axisRight.enabled = (true);
          axisRight
            ..typeface = Util.REGULAR
            ..drawGridLines = (false)
            ..textColor = (FintnessAppTheme.grey.withOpacity(0.5))
            ..position = (YAxisLabelPosition.INSIDE_CHART)
            ..axisLineColor = (Colors.transparent);
        },
        legendSettingFunction: (legend, controller) {
          (controller as LineChartController).setViewPortOffsets(5, 20, 5, 3);
          legend.enabled = (false);
          var data = (controller as LineChartController).data;
          if (data != null) {
            var formatter = data.getDataSetByIndex(0).getFillFormatter();
          }
        },
        xAxisSettingFunction: (xAxis, controller) {
          xAxis.enabled = (false);
          // xAxis
          //   ..setLabelCount2(6, false)
          //   ..position = XAxisPosition.BOTTOM_INSIDE
          //   ..drawGridLines = (false);
        },
        drawGridBackground: false,
        dragXEnabled: false,
        dragYEnabled: false,
        scaleXEnabled: false,
        scaleYEnabled: false,
        pinchZoomEnabled: true,
        gridBackColor: Colors.red,
        description: desc);
  }

  void _initLineData() async {
    List<Entry> values = List();

    for (int i = 0; i < widget.data_chart.length; i++) {
      values.add(
          Entry(x: i.toDouble(), y: widget.data_chart[i]['value'].toDouble()));
    }

    LineDataSet set1;
    // create a dataset and give it a type
    set1 = LineDataSet(values, "Data");

    set1.setMode(Mode.CUBIC_BEZIER);
    set1.setCubicIntensity(0.2);
    set1.setDrawFilled(true);
    set1.setDrawCircles(true);
    set1.setLineWidth(1.8);
    set1.setCircleRadius(4);
    set1.setCircleColor(Color(0xFF1CC7DE));
    set1.setHighLightColor(Colors.black);
    set1.setColor1(Color(0xFF1CC7DE));
    set1.setFillColor(Color(0xFF1CC7DE));
    set1.setFillAlpha(100);
    set1.setDrawHorizontalHighlightIndicator(true);

    // create a data object with the data sets
    controller.data = LineData.fromList(List()..add(set1))
      ..setValueTypeface(Util.LIGHT)
      ..setValueTextSize(9)
      ..setDrawValues(false);

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    _initLineData();
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        children: [
          Text(
            widget.name,
            style: TextStyle(
              color: FintnessAppTheme.grey.withOpacity(0.5),
              fontSize: 14.0,
            ),
          ),
          Expanded(
            child: LineChart(controller),
          ),
        ],
      ),
    );
  }
}

// import 'dart:math';
// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:mp_chart/mp/chart/line_chart.dart';
// import 'package:mp_chart/mp/controller/line_chart_controller.dart';
// import 'package:mp_chart/mp/core/data/line_data.dart';
// import 'package:mp_chart/mp/core/data_set/line_data_set.dart';
// import 'package:mp_chart/mp/core/description.dart';
// import 'package:mp_chart/mp/core/entry/entry.dart';
// import 'package:mp_chart/mp/core/enums/mode.dart';
// import 'package:mp_chart/mp/core/enums/y_axis_label_position.dart';
// import '../helper/util.dart';
// import 'package:http/http.dart' as http;

// import '../helper/custom_theme.dart';

// class LineChartView extends StatefulWidget {
//   final String name;
//   final String id;
//   const LineChartView({this.name, this.id});

//   @override
//   _LineChartViewState createState() => _LineChartViewState();
// }

// class _LineChartViewState extends State<LineChartView> {
//   var random = Random(1);
//   int _count = 45;
//   double _range = 100.0;

//   LineChartController controller;
//   @override
//   void initState() {
//     _initController();
//     _initLineData(_count, _range);
//     super.initState();
//   }

//   void _initController() {
//     var desc = Description()..enabled = false;
//     controller = LineChartController(
//         axisLeftSettingFunction: (axisLeft, controller) {
//           axisLeft
//             ..typeface = Util.REGULAR
//             ..setLabelCount2(6, false)
//             ..textColor = (FintnessAppTheme.grey.withOpacity(0.5))
//             ..textSize = 10
//             ..position = (YAxisLabelPosition.INSIDE_CHART)
//             ..drawGridLines = (false)
//             ..axisLineColor = (Colors.transparent);
//         },
//         axisRightSettingFunction: (axisRight, controller) {
//           axisRight.enabled = (true);
//           axisRight
//             ..typeface = Util.REGULAR
//             ..drawGridLines = (false)
//             ..textColor = (FintnessAppTheme.grey.withOpacity(0.5))
//             ..position = (YAxisLabelPosition.INSIDE_CHART)
//             ..axisLineColor = (Colors.transparent);
//         },
//         legendSettingFunction: (legend, controller) {
//           (controller as LineChartController).setViewPortOffsets(5, 20, 5, 3);
//           legend.enabled = (false);
//           var data = (controller as LineChartController).data;
//           if (data != null) {
//             var formatter = data.getDataSetByIndex(0).getFillFormatter();
//           }
//         },
//         xAxisSettingFunction: (xAxis, controller) {
//           xAxis.enabled = (false);
//           // xAxis
//           //   ..setLabelCount2(6, false)
//           //   ..position = XAxisPosition.BOTTOM_INSIDE
//           //   ..drawGridLines = (false);
//         },
//         drawGridBackground: false,
//         dragXEnabled: false,
//         dragYEnabled: false,
//         scaleXEnabled: false,
//         scaleYEnabled: false,
//         pinchZoomEnabled: true,
//         gridBackColor: Colors.red,
//         description: desc);
//   }

//   void _initLineData(int count, double range) async {
//     List<Entry> values = List();
//     String url =
//         'http://iot.elefante.co.id/get_sensor_history_phone/${widget.id}';
//     final response =
//         await http.get(url, headers: {"Accept": "application/json"});

//     Map<String, dynamic> dataJson = jsonDecode(response.body);

//     for (int i = 0; i < dataJson['history'].length; i++) {
//       values.add(Entry(
//           x: i.toDouble(), y: dataJson['history'][i]['value'].toDouble()));
//     }

//     LineDataSet set1;
//     // create a dataset and give it a type
//     set1 = LineDataSet(values, "Data");

//     set1.setMode(Mode.CUBIC_BEZIER);
//     set1.setCubicIntensity(0.2);
//     set1.setDrawFilled(true);
//     set1.setDrawCircles(true);
//     set1.setLineWidth(1.8);
//     set1.setCircleRadius(4);
//     set1.setCircleColor(Color(0xFF1CC7DE));
//     set1.setHighLightColor(Colors.black);
//     set1.setColor1(Color(0xFF1CC7DE));
//     set1.setFillColor(Color(0xFF1CC7DE));
//     set1.setFillAlpha(100);
//     set1.setDrawHorizontalHighlightIndicator(true);

//     // create a data object with the data sets
//     controller.data = LineData.fromList(List()..add(set1))
//       ..setValueTypeface(Util.LIGHT)
//       ..setValueTextSize(9)
//       ..setDrawValues(false);

//     setState(() {});
//   }

//   Widget _initLineChart() {
//     var lineChart = LineChart(controller);
//     controller.animator
//       ..reset()
//       ..animateXY1(2000, 2000);
//     return lineChart;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 8.0),
//       child: Column(
//         children: [
//           Text(
//             widget.name,
//             style: TextStyle(
//               color: FintnessAppTheme.grey.withOpacity(0.5),
//               fontSize: 14.0,
//             ),
//           ),
//           Expanded(
//             child: LineChart(controller),
//           ),
//         ],
//       ),
//     );
//   }
// }
