import 'package:flutter/material.dart';

import '../models/control.dart';

class ControlItem extends StatelessWidget {
  final Control control_item;
  final int index;
  final Function bottomSheetHandler;
  final Function controlHandler;
  final Function sendHandler;
  final Function deleteHandler;

  ControlItem(this.control_item, this.index, this.bottomSheetHandler,
      this.controlHandler, this.sendHandler, this.deleteHandler);

  @override
  Widget build(BuildContext context) {
    Widget getIconType(ControlType type) {
      if (type == ControlType.onof) {
        return CircleAvatar(
          backgroundColor: Theme.of(context).accentColor,
          child: Icon(Icons.power_settings_new,
              color: Theme.of(context).textTheme.bodyText2.color),
        );
      }
      if (type == ControlType.slider) {
        return CircleAvatar(
          backgroundColor: Theme.of(context).accentColor,
          child: Icon(Icons.compare_arrows,
              color: Theme.of(context).textTheme.bodyText2.color),
        );
      }
      if (type == ControlType.value) {
        return CircleAvatar(
          backgroundColor: Theme.of(context).accentColor,
          child: Icon(Icons.input,
              color: Theme.of(context).textTheme.bodyText2.color),
        );
      }
    }

    return Dismissible(
      key: ValueKey(control_item.id),
      background: Container(
        margin: EdgeInsets.only(bottom: 10),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.symmetric(horizontal: 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Theme.of(context).errorColor,
        ),
        // color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Theme.of(context).iconTheme.color,
          size: 20,
        ),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('Are you sure ?'),
            content: Text('Do you want to remove this item?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(ctx).pop(false);
                },
                child: Text("No"),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(ctx).pop(true);
                },
                child: Text("Yes"),
              )
            ],
          ),
        );
      },
      onDismissed: (direction) {
        deleteHandler(control_item.id);
      },
      child: Card(
        margin: EdgeInsets.only(bottom: 10),
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: ListTile(
            title: Text(control_item.name),
            leading: getIconType(control_item.type),
            trailing: IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                bottomSheetHandler(context, control_item.id);
              },
            ),
            onTap: () {
              controlHandler(context, control_item.id, sendHandler);
            },
            // trailing: Container(
            //   width: 100,
            //   child: Row(
            //     children: <Widget>[
            //       IconButton(
            //         icon: Icon(Icons.settings),
            //         onPressed: () {
            //           bottomSheetHandler(context, control_item.id);
            //         },
            //       ),
            //       IconButton(
            //         icon: Icon(Icons.send),
            //         onPressed: () {
            //           sendHandler(control_item.id);
            //         },
            //         color: Colors.green,
            //       ),
            //     ],
            //   ),
            // ),
          ),
        ),
      ),
    );
  }
}
