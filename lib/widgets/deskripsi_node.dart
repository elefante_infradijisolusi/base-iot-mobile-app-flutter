import 'package:flutter/material.dart';
import 'dart:math';

import '../helper/custom_theme.dart';

class DetailView extends StatelessWidget {
  final String name;
  final int numSensor;
  final String kategori;
  final dynamic status;
  final List<Map> dataSensorAvg;
  final Map deviceStatus;

  DetailView({
    this.name,
    this.numSensor,
    this.kategori,
    this.status,
    this.dataSensorAvg,
    this.deviceStatus,
  });

  Color findColor(String message) {
    if (message.toLowerCase().contains("heat") ||
        message.toLowerCase().contains("fuse")) {
      return Colors.red;
    } else if (message.toLowerCase().contains("balance") ||
        message.toLowerCase().contains("value")) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  Widget getStatusWidget(context, code) {
    if (code > 2) {
      return Text(
        "Danger",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: FintnessAppTheme.fontName,
          fontWeight: FontWeight.w600,
          fontSize: 32,
          color: Theme.of(context).errorColor,
        ),
      );
    } else if (code <= 2 && code > 0) {
      return Text(
        "Warning",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: FintnessAppTheme.fontName,
          fontWeight: FontWeight.w600,
          fontSize: 32,
          color: Colors.orange,
        ),
      );
    } else {
      return Text(
        "Safe",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: FintnessAppTheme.fontName,
          fontWeight: FontWeight.w600,
          fontSize: 32,
          color: Colors.green,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          elevation: 2,
          margin: EdgeInsets.all(8),
          child: Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 16, right: 24),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 4, bottom: 8, top: 16),
                        child: Text(
                          name.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: FintnessAppTheme.fontName,
                              fontWeight: FontWeight.w600,
                              fontSize: 24,
                              letterSpacing: -0.1,
                              color: Theme.of(context).accentColor),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Icon(
                                    Icons.wifi_tethering,
                                    color:
                                        FintnessAppTheme.grey.withOpacity(0.5),
                                    size: 16,
                                  ),
                                  Icon(
                                    Icons.category,
                                    color:
                                        FintnessAppTheme.grey.withOpacity(0.5),
                                    size: 16,
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      '${numSensor} Sensors',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: FintnessAppTheme.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                        letterSpacing: 0.0,
                                        color: FintnessAppTheme.grey
                                            .withOpacity(0.5),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Text(
                                      kategori,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: FintnessAppTheme.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                        letterSpacing: 0.0,
                                        color: FintnessAppTheme.grey
                                            .withOpacity(0.5),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4, bottom: 3),
                            child: getStatusWidget(context, status["code"]),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 24, right: 24, top: 8, bottom: 8),
                  child: Container(
                    height: 2,
                    decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: 24, right: 24, top: 8, bottom: 16),
                  child: Row(children: <Widget>[
                    ...dataSensorAvg.sublist(0, 4).map<Widget>((elemen) {
                      if (dataSensorAvg.indexOf(elemen) != 2) {
                        return Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                '${(elemen["value"].reduce((a, b) => a + b) / elemen["value"].length).toStringAsFixed(1)} ${elemen["unit"].toUpperCase()}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontFamily: FintnessAppTheme.fontName,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  letterSpacing: -0.2,
                                  color: FintnessAppTheme.grey.withOpacity(0.5),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 6),
                                child: Text(
                                  'Avg. ${elemen["parameter"][0].toUpperCase()}${elemen["parameter"].substring(1)}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: FintnessAppTheme.fontName,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12,
                                    color: Theme.of(context).accentColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Container();
                      }
                    }).toList(),
                  ]),
                ),
              ],
            ),
          ),
        ),
        // Container(
        //   child: Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: deviceStatus["message"]
        //         .reversed
        //         .toList()
        //         .map<Widget>((item) => Center(
        //               child: Container(
        //                 padding: EdgeInsets.all(4.0),
        //                 margin: EdgeInsets.only(right: 5),
        //                 decoration: BoxDecoration(
        //                   borderRadius: BorderRadius.circular(10),
        //                   border: Border.all(
        //                     color: findColor(item),
        //                     width: 1.0,
        //                   ),
        //                 ),
        //                 child: Center(
        //                     child: Text(
        //                   item,
        //                   style: TextStyle(
        //                       color: findColor(item),
        //                       fontSize: 10,
        //                       fontWeight: FontWeight.w400),
        //                 )),
        //               ),
        //             ))
        //         .toList(),
        //   ),
        // ),
      ],
    );
  }
}
