import 'package:flutter/material.dart';

import '../models/color_converter.dart';

class SensorItem extends StatelessWidget {
  final int index;
  final String name;
  final String value;
  final String unit;
  final int code;
  final Function clickHandler;

  SensorItem(
      {this.index,
      this.name,
      this.value,
      this.clickHandler,
      this.unit,
      this.code});

  CircleAvatar getAvatar(String unit) {
    if (unit.toLowerCase().contains('v')) {
      return CircleAvatar(
        backgroundColor: Color.fromRGBO(89, 10, 49, 1.0),
        child: Text(
          "V",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    } else if (unit.toLowerCase().contains('a')) {
      return CircleAvatar(
        backgroundColor: Color.fromRGBO(49, 17, 100, 1.0),
        child: Text(
          "A",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    } else {
      return CircleAvatar(
        backgroundColor: Color.fromRGBO(0, 77, 38, 1.0),
        child: Text(
          "T",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }
  }

  Color selecColor(code) {
    if (code == 2) {
      return Colors.red;
    } else if (code == 1) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(bottom: 10),
      child: ListTile(
        contentPadding: EdgeInsets.only(right: 20, left: 20),
        onTap: clickHandler,
        leading: getAvatar(unit),
        title: Text(
          '${name[0].toUpperCase()}${name.substring(1)}',
        ),
        trailing: Container(
          padding: EdgeInsets.all(6.0),
          margin: EdgeInsets.only(right: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: selecColor(code),
              width: 1.0,
            ),
          ),
          child: Text(
            '${value} ${unit.contains('&#176;') ? unit.replaceAll('&#176;', '\u00b0') : unit}',
            style: TextStyle(
              color: selecColor(code),
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
