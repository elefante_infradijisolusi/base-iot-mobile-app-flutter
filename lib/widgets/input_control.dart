import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/controls.dart';
import '../models/control.dart';

class AddControl extends StatefulWidget {
  final String id;
  const AddControl(this.id);

  @override
  _AddControlState createState() => _AddControlState();
}

class _AddControlState extends State<AddControl> {
  final _ipFocusNode = FocusNode();
  final _portFocusNode = FocusNode();
  final _topicFocusNode = FocusNode();
  final _valueFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();

  var _editedValues = Control(
      id: "",
      name: "",
      ip: "",
      port: "",
      topic: "",
      value: 0,
      min: 0,
      max: 0,
      step: 0,
      type: ControlType.onof);

  void _saveForm(String id) {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    id == null
        ? Provider.of<Controls>(context, listen: false)
            .addControl(_editedValues)
        : Provider.of<Controls>(context, listen: false)
            .updateProduct(id, _editedValues);
    Navigator.of(context).pop();
  }

  @override
  void dispose() {
    _ipFocusNode.dispose();
    _portFocusNode.dispose();
    _topicFocusNode.dispose();
    _valueFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _initValues = widget.id != null
        ? Provider.of<Controls>(context, listen: false).findById(widget.id)
        : Control(
            id: "",
            name: "",
            ip: "",
            port: "",
            topic: "",
            value: 0,
            max: 0,
            min: 0,
            step: 0,
            type: ControlType.onof);
    _editedValues = widget.id != null
        ? Provider.of<Controls>(context, listen: false).findById(widget.id)
        : Control(
            id: "",
            name: "",
            ip: "",
            port: "",
            topic: "",
            value: 0,
            max: 0,
            min: 0,
            step: 0,
            type: ControlType.onof);
    return SingleChildScrollView(
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(
              20.0, 20.0, 20.0, 20.0), // content padding
          child: Form(
            key: _form,
            child: Column(
              children: <Widget>[
                Container(
                  child: Text(
                    "Add Control",
                    style: TextStyle(
                        color: Theme.of(context).textTheme.bodyText1.color,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                TextFormField(
                  initialValue: _initValues.name,
                  decoration: InputDecoration(labelText: 'Name'),
                  textInputAction: TextInputAction.next,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_ipFocusNode);
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please provide a value.';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _editedValues = Control(
                        id: _editedValues.id,
                        name: value,
                        ip: _editedValues.ip,
                        port: _editedValues.port,
                        topic: _editedValues.topic,
                        value: _editedValues.value,
                        max: _editedValues.max,
                        min: _editedValues.min,
                        step: _editedValues.step,
                        type: _editedValues.type);
                  },
                ),
                DropdownButtonFormField(
                  value: _initValues.type,
                  decoration: InputDecoration(labelText: 'Type'),
                  items: [
                    DropdownMenuItem(
                      child: Text('Switch'),
                      value: ControlType.onof,
                    ),
                    DropdownMenuItem(
                      child: Text('Input Value'),
                      value: ControlType.value,
                    ),
                    DropdownMenuItem(
                      child: Text('Slider'),
                      value: ControlType.slider,
                    ),
                  ],
                  onChanged: (value) {
                    setState(() {
                      _editedValues = Control(
                          id: _editedValues.id,
                          name: _editedValues.name,
                          ip: _editedValues.ip,
                          port: _editedValues.port,
                          topic: _editedValues.topic,
                          value: _editedValues.value,
                          max: _editedValues.max,
                          min: _editedValues.min,
                          step: _editedValues.step,
                          type: value);
                    });
                  },
                  isExpanded: true,
                  onSaved: (value) {
                    _editedValues = Control(
                        id: _editedValues.id,
                        name: _editedValues.name,
                        ip: _editedValues.ip,
                        port: _editedValues.port,
                        topic: _editedValues.topic,
                        value: _editedValues.value,
                        max: _editedValues.max,
                        min: _editedValues.min,
                        step: _editedValues.step,
                        type: value);
                  },
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: TextFormField(
                          initialValue: _initValues.ip,
                          decoration: InputDecoration(labelText: 'IP Address'),
                          textInputAction: TextInputAction.next,
                          focusNode: _ipFocusNode,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).requestFocus(_portFocusNode);
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter a IP.';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _editedValues = Control(
                                id: _editedValues.id,
                                name: _editedValues.name,
                                ip: value,
                                port: _editedValues.port,
                                topic: _editedValues.topic,
                                value: _editedValues.value,
                                max: _editedValues.max,
                                min: _editedValues.min,
                                step: _editedValues.step,
                                type: _editedValues.type);
                          },
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: TextFormField(
                          initialValue: _initValues.port,
                          decoration: InputDecoration(labelText: 'Port'),
                          textInputAction: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          focusNode: _portFocusNode,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context)
                                .requestFocus(_topicFocusNode);
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter a Port.';
                            }
                            if (int.tryParse(value) == null) {
                              return 'Please enter a Port.';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            _editedValues = Control(
                                id: _editedValues.id,
                                name: _editedValues.name,
                                ip: _editedValues.ip,
                                port: value,
                                topic: _editedValues.topic,
                                value: _editedValues.value,
                                max: _editedValues.max,
                                min: _editedValues.min,
                                step: _editedValues.step,
                                type: _editedValues.type);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                TextFormField(
                  initialValue: _initValues.topic,
                  decoration: InputDecoration(labelText: 'Topic'),
                  textInputAction: TextInputAction.next,
                  focusNode: _topicFocusNode,
                  onFieldSubmitted: (_) {
                    FocusScope.of(context).requestFocus(_valueFocusNode);
                  },
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please provide a value.';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _editedValues = Control(
                        id: _editedValues.id,
                        name: _editedValues.name,
                        ip: _editedValues.ip,
                        port: _editedValues.port,
                        topic: value,
                        value: _editedValues.value,
                        max: _editedValues.max,
                        min: _editedValues.min,
                        step: _editedValues.step,
                        type: _editedValues.type);
                  },
                ),
                (_editedValues.type != ControlType.onof)
                    ? Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: TextFormField(
                                initialValue: _initValues.min.toString(),
                                decoration: InputDecoration(labelText: 'Min'),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.unspecified,
                                // focusNode: _ipFocusNode,
                                // onFieldSubmitted: (_) {
                                //   FocusScope.of(context).requestFocus(_portFocusNode);
                                // },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Min Value';
                                  }
                                  if (double.tryParse(value) == null) {
                                    return 'Min Value';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedValues = Control(
                                      id: _editedValues.id,
                                      name: _editedValues.name,
                                      ip: _editedValues.ip,
                                      port: _editedValues.port,
                                      topic: _editedValues.topic,
                                      value: _editedValues.value,
                                      max: _editedValues.max,
                                      min: double.parse(value),
                                      step: _editedValues.step,
                                      type: _editedValues.type);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: TextFormField(
                                initialValue: _initValues.max.toString(),
                                decoration: InputDecoration(labelText: 'Max'),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.unspecified,
                                // keyboardType: TextInputType.number,
                                // focusNode: _portFocusNode,
                                // onFieldSubmitted: (_) {
                                //   FocusScope.of(context)
                                //       .requestFocus(_topicFocusNode);
                                // },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Max Value';
                                  }
                                  if (double.tryParse(value) == null) {
                                    return 'Max Value';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedValues = Control(
                                      id: _editedValues.id,
                                      name: _editedValues.name,
                                      ip: _editedValues.ip,
                                      port: _editedValues.port,
                                      topic: _editedValues.topic,
                                      value: _editedValues.value,
                                      max: double.parse(value),
                                      min: _editedValues.min,
                                      step: _editedValues.step,
                                      type: _editedValues.type);
                                },
                              ),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: TextFormField(
                                initialValue: _initValues.step.toString(),
                                decoration: InputDecoration(labelText: 'Step'),
                                keyboardType: TextInputType.number,
                                textInputAction: TextInputAction.unspecified,
                                // keyboardType: TextInputType.number,
                                // focusNode: _portFocusNode,
                                // onFieldSubmitted: (_) {
                                //   FocusScope.of(context)
                                //       .requestFocus(_topicFocusNode);
                                // },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Step Value';
                                  }
                                  if (double.tryParse(value) == null) {
                                    return 'Step Value';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  _editedValues = Control(
                                      id: _editedValues.id,
                                      name: _editedValues.name,
                                      ip: _editedValues.ip,
                                      port: _editedValues.port,
                                      topic: _editedValues.topic,
                                      value: _editedValues.value,
                                      max: _editedValues.max,
                                      min: _editedValues.min,
                                      step: double.parse(value),
                                      type: _editedValues.type);
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox(
                        height: 0,
                      ),
                // TextFormField(
                //   initialValue: _initValues.value.toString(),
                //   decoration: InputDecoration(labelText: 'Value'),
                //   textInputAction: TextInputAction.done,
                //   keyboardType: TextInputType.number,
                //   focusNode: _valueFocusNode,
                //   onFieldSubmitted: (_) {
                //     _saveForm(widget.id);

                //     Scaffold.of(context).showSnackBar(
                //       SnackBar(
                //         content: widget.id == null
                //             ? Text("Item Added")
                //             : Text("Item Edited"),
                //         duration: Duration(seconds: 1),
                //       ),
                //     );
                //   },
                //   validator: (value) {
                //     if (value.isEmpty) {
                //       return 'Please provide a value.';
                //     }
                //     if (double.tryParse(value) == null) {
                //       return 'Please provide a value.';
                //     }
                //     return null;
                //   },
                //   onSaved: (value) {
                //     _editedValues = Control(
                //         id: _editedValues.id,
                //         name: _editedValues.name,
                //         ip: _editedValues.ip,
                //         port: _editedValues.port,
                //         topic: _editedValues.topic,
                //         value: double.parse(value),
                //         max: _editedValues.max,
                //         min: _editedValues.min,
                //         step: _editedValues.step,
                //         type: _editedValues.type);
                //   },
                // ),
                Container(
                  padding: const EdgeInsets.fromLTRB(0, 20.0, 0.0, 0.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        // cancel
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                'Cancel',
                                style: TextStyle(
                                  color: Theme.of(context).errorColor,
                                ),
                              )),
                        ),

                        // Submit
                        Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton.icon(
                              icon: Icon(Icons.add),
                              onPressed: () {
                                _saveForm(widget.id);
                              },
                              label: Text('Submit')),
                        ),
                      ]),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
