import 'dart:convert';

class Task {
  const Task({
    this.id,
    this.title,
    this.message,
    this.priority,
    this.startDate,
    this.endDate,
    this.checkList,
    this.comment,
    this.status,
    this.targetLocation,
    this.actualLocation,
  });
  final String id;
  final String title;
  final String message;
  final String priority;
  final String comment;
  final String status;
  final DateTime startDate;
  final DateTime endDate;
  final Map<String, dynamic> checkList;
  final Map<String, dynamic> targetLocation;
  final Map<String, dynamic> actualLocation;

  List<dynamic> getChecklist() {
    final List data = [];
    checkList.forEach((key, value) {
      data.add([
        {"name": key, "value": value}
      ]);
    });

    return data;
  }
}

class ListTask {
  List<Task> _tasks = [];

  void createTask(List<dynamic> data) {
    _tasks = data
        .map<Task>((e) => Task(
              id: e["_id"],
              title: e["title"],
              message: e["message"],
              comment: e["comment"],
              status: e["status"],
              priority: e["priority"],
              targetLocation: e["targetLocation"],
              actualLocation: {},
              checkList: json.decode(e["checkList"]),
              endDate: DateTime.parse(e["endDate"]),
              startDate: DateTime.parse(e["startDate"]),
            ))
        .toList();
  }

  Task getTask(int index) {
    return _tasks[index];
  }

  List<Task> getTasks() {
    return [..._tasks];
  }

  int getLength() {
    return _tasks.length;
  }
}
