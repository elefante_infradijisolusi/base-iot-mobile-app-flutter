// import 'dart:html';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Item {
  const Item({this.sensor, this.type, this.id});
  final String sensor;
  final int type;
  final String id;
}

class CallibrationReport {
  CallibrationReport({this.url});
  final String url;
  Map _data = {};
  List<Item> _listSensor;
  List<double> _score = [];

  Future<Map> fetchSensorList() async {
    final response = await http.get('http://${url}/getData/all');
    final responseData = json.decode(response.body);

    return responseData;
  }

  void initListSensor(List<Item> sensorList) {
    _listSensor = sensorList;
  }

  List<Item> getSensorList() {
    return _listSensor;
  }

  void addReport(String idSensor, double score, String name, Map value) {
    value.putIfAbsent("name", () => name);
    _data.putIfAbsent(idSensor, () => value);
    _score.add(score);
  }

  getScore() {
    if (_score.length > 0) {
      return _score.map((m) => m).reduce((a, b) => a + b) / _score.length;
    } else {
      return -1;
    }
  }

  Map getData() {
    return {..._data};
  }

  void clear() {
    _data = {};
    _score = [];
  }
}

class ItemCallibration {
  List _value = [];
  Map _oldJson = {};
  Map _newJson = {};
  double score = 0;

  Map<String, dynamic> _token = {"token": null, "count": 0};

  void generateNewJson(String type) {
    if (_value.length > 0) {
      if (type == "linier" || type == "linear") {
        double meanSensorValue =
            _value.map((m) => m["rawValue"]).reduce((a, b) => a + b) /
                _value.length;
        double meanActualValue = _value
                .map((m) => m["actualValue"] == null ? 0 : m["actualValue"])
                .reduce((a, b) => a + b) /
            _value.length;
        _newJson["a"] = (meanActualValue / meanSensorValue).toStringAsFixed(4);
      } else if ((type == "non-linier" || type == "nonLinear") &&
          _value.length == 2) {
        var meanSensorValue = _value.map<double>((m) => m["rawValue"]).toList();
        var meanActualValue =
            _value.map<double>((m) => m["actualValue"]).toList();
        if (meanActualValue.contains(null)) {
        } else {
          if (meanActualValue.length == 2 && meanSensorValue.length == 2) {
            var a = (meanActualValue[0] - meanActualValue[1]) /
                (meanSensorValue[0] - meanSensorValue[1]);
            var b = meanActualValue[0] - (a * meanSensorValue[0]);
            _newJson["a"] = a.toStringAsFixed(4);
            _newJson["b"] = b.toStringAsFixed(4);
          }
        }
      } else if (type == "timer") {}
    }
  }

  String randomString(int length) {
    var codeUnits = new List.generate(length, (index) {
      return 0;
    });
    return new String.fromCharCodes(codeUnits);
  }

  String gain2string(String gain) {
    final stringGain = (double.parse(gain) * 10000).toInt().toString();
    String gainSixDigit;
    if (stringGain.length < 5) {
      final zeroString = randomString(5 - stringGain.length);
      gainSixDigit = "$zeroString$stringGain";
    } else {
      gainSixDigit = stringGain.substring(0, 5);
    }
    return gainSixDigit;
  }

  Future<dynamic> postData(String id) async {
    if (_token["token"] == null || _token["count"] == 0) {
      final loginResponse = await http.post('http://192.168.4.1/getLogin',
          body: jsonEncode({"key": "1o2Ou"}));
      final loginData = json.decode(loginResponse.body);
      _token["token"] = loginData["token"];
      _token["count"] = loginData["count"];
    }

    dynamic response;
    if (_newJson.length == 1) {
      final msg = {
        "id": id,
        "type": (_newJson.length - 1),
        "a": gain2string(_newJson["a"]),
        "b": "00000",
        "token": _token["token"]
      };
      response = await http.post(
        'http://192.168.4.1/setData',
        body: jsonEncode(msg),
      );

      final dataResponse = json.decode(response.body);
      _token["count"] = dataResponse["count"];
    } else {
      final msg = {
        "id": id,
        "type": (_newJson.length - 1),
        "a": gain2string(_newJson["a"]),
        "b": gain2string(_newJson["b"]),
        "token": _token["token"]
      };
      response = await http.post(
        'http://192.168.4.1/setData',
        body: jsonEncode(msg),
      );
      final dataResponse = json.decode(response.body);
      _token["count"] = dataResponse["count"];
    }

    return response.body;
  }

  Future<dynamic> addRow(String type, String idSensor) async {
    var response;

    if (_token["token"] == null || _token["count"] < 1) {
      final loginResponse = await http.post('http://192.168.4.1/getLogin',
          body: jsonEncode({"key": "1o2Ou"}));

      final loginData = json.decode(loginResponse.body);
      _token["token"] = loginData["token"];
      _token["count"] = loginData["count"];
    }
    if (type == "linier") {
      response = await http.post(
        'http://192.168.4.1/getData',
        body: jsonEncode({"id": idSensor, "type": 0, "token": _token["token"]}),
      );
      final dataResponse = json.decode(response.body);

      _token["count"] = dataResponse["count"];

      if (dataResponse["value"] != null &&
          double.tryParse(dataResponse["value"]) != null) {
        _oldJson["a"] = double.parse(dataResponse["a"]) / 10000;
        _newJson["a"] = null;
        _value.add(
          {
            "rawValue": double.parse(dataResponse["value"]),
            "id": DateTime.now().toIso8601String()
          },
        );
      }
    } else if (type == "non-linier") {
      response = await http.post(
        'http://192.168.4.1/getData',
        body: jsonEncode({"id": idSensor, "type": 1, "token": _token["token"]}),
      );
      final dataResponse = json.decode(response.body);

      _token["count"] = dataResponse["count"];

      if (dataResponse["value"] != null &&
          double.tryParse(dataResponse["value"]) != null) {
        _oldJson["a"] = double.parse(dataResponse["a"]) / 10000;
        _oldJson["b"] = double.parse(dataResponse["b"]) / 10000;
        _newJson["a"] = null;
        _newJson["b"] = null;
        _value.add(
          {
            "rawValue": double.parse(dataResponse["value"]),
            "id": DateTime.now().toIso8601String()
          },
        );
      }

      // response =
      //     await http.get('http://192.168.4.1/getData/getData');
      // final actualArray = json.decode(response.body)["actual"];
      // final meanValue = actualArray.map((m) => m).reduce((a, b) => a + b) /
      //     actualArray.length;
      // _oldJson["a"] = json.decode(response.body)["a"];
      // _oldJson["b"] = json.decode(response.body)["b"];
      // _newJson["a"] = null;
      // _newJson["b"] = null;
      // _value.add(
      //   {"rawValue": meanValue, "id": DateTime.now().toIso8601String()},
      // );
    } else if (type == "timer") {
      _oldJson["timer"] = 300;
      _oldJson["total-timer"] = 1000;
      _oldJson["saturation"] = 50;

      _newJson["power-factor"] =
          100 * _oldJson["timer"] / _oldJson["total-timer"];
    }

    if (_value.length > 0) {
      generateNewJson(type);
    }

    return response;
  }

  Widget getOldData(String type) {
    List<Widget> rowItem = [];
    for (var key in _oldJson.keys) {
      rowItem.add(
        Text(
          "${key} : ${_oldJson[key]}",
          style: TextStyle(
            color: Colors.red,
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          (type != "timer")
              ? Text(
                  "Old Data :",
                  style:
                      TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
                )
              : SizedBox(
                  width: 0.0,
                ),
          ...rowItem
        ],
      ),
    );
  }

  Widget getNewData(String type) {
    List<Widget> rowItem = [];
    for (var key in _newJson.keys) {
      rowItem.add(
        Text(
          "${key} : ${_newJson[key]}",
          style: TextStyle(
            color: Colors.green,
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          (type != "timer")
              ? Text(
                  "New Data :",
                  style: TextStyle(
                      color: Colors.green, fontWeight: FontWeight.bold),
                )
              : SizedBox(
                  width: 0.0,
                ),
          ...rowItem
        ],
      ),
    );
  }

  getVal() {
    return [..._value];
  }

  getScore() {
    var score = [];

    for (var key in _oldJson.keys) {
      score.add((double.parse(_newJson[key]) - _oldJson[key]));
    }
    return score.map((m) => m).reduce((a, b) => a + b) / score.length;
  }

  getSendData(String id) {
    return {..._newJson, "idSensor": id};
  }

  deleteByValue(value, type) {
    _value.remove(value);
    generateNewJson(type);
  }

  updateValue(elemen, value, type) {
    int index = _value.indexOf(elemen);
    _value[index]["actualValue"] = double.parse(value);
    generateNewJson(type);
  }

  clear() {
    _value = [];
    _oldJson = {};
    _newJson = {};
  }
}

// class ItemCallibration {
//   List _value = [];
//   Map _oldJson = {};
//   Map _newJson = {};
//   double score = 0;

//   Map<String, dynamic> _token = {"token": null, "count": 0};

//   void generateNewJson(String type) {
//     if (_value.length > 0) {
//       if (type == "linier" || type == "linear") {
//         double meanSensorValue =
//             _value.map((m) => m["rawValue"]).reduce((a, b) => a + b) /
//                 _value.length;
//         double meanActualValue = _value
//                 .map((m) => m["actualValue"] == null ? 0 : m["actualValue"])
//                 .reduce((a, b) => a + b) /
//             _value.length;
//         _newJson["a"] = (meanActualValue / meanSensorValue).toStringAsFixed(4);
//       } else if ((type == "non-linier" || type == "nonLinear") &&
//           _value.length == 2) {
//         var meanSensorValue = _value.map<double>((m) => m["rawValue"]).toList();
//         var meanActualValue =
//             _value.map<double>((m) => m["actualValue"]).toList();
//         if (meanActualValue.contains(null)) {
//         } else {
//           if (meanActualValue.length == 2 && meanSensorValue.length == 2) {
//             var a = (meanActualValue[0] - meanActualValue[1]) /
//                 (meanSensorValue[0] - meanSensorValue[1]);
//             var b = meanActualValue[0] - (a * meanSensorValue[0]);
//             _newJson["a"] = a.toStringAsFixed(4);
//             _newJson["b"] = b.toStringAsFixed(4);
//           }
//         }
//       } else if (type == "timer") {}
//     }
//   }

//   String randomString(int length) {
//     var codeUnits = new List.generate(length, (index) {
//       return 0;
//     });
//     return new String.fromCharCodes(codeUnits);
//   }

//   String gain2string(String gain) {
//     final stringGain = (double.parse(gain) * 10000).toInt().toString();
//     String gainSixDigit;
//     if (stringGain.length < 5) {
//       final zeroString = randomString(5 - stringGain.length);
//       gainSixDigit = "$zeroString$stringGain";
//     } else {
//       gainSixDigit = stringGain.substring(0, 5);
//     }
//     return gainSixDigit;
//   }

//   Future<dynamic> postData(String id) async {
//     if (_token["token"] == null || _token["count"] == 0) {
//       final loginResponse = await http.post('http://192.168.4.1/getLogin',
//           body: jsonEncode({"key": "1o2Ou"}));
//       final loginData = json.decode(loginResponse.body);
//       _token["token"] = loginData["token"];
//       _token["count"] = loginData["count"];
//     }

//     dynamic response;
//     if (_newJson.length == 1) {
//       final msg = {
//         "id": id,
//         "type": (_newJson.length - 1),
//         "a": gain2string(_newJson["a"]),
//         "b": "00000",
//         "token": _token["token"]
//       };
//       print("android = ${msg.toString()}");
//       response = await http.post(
//         'http://192.168.4.1/setData',
//         body: jsonEncode(msg),
//       );

//       final dataResponse = json.decode(response.body);
//       print("esp = ${dataResponse.toString()}");
//       _token["count"] = dataResponse["count"];
//     } else {
//       final msg = {
//         "id": id,
//         "type": (_newJson.length - 1),
//         "a": gain2string(_newJson["a"]),
//         "b": gain2string(_newJson["b"]),
//         "token": _token["token"]
//       };
//       response = await http.post(
//         'http://192.168.4.1/setData',
//         body: jsonEncode(msg),
//       );
//       print("android = ${msg.toString()}");
//       final dataResponse = json.decode(response.body);
//       print("esp = ${dataResponse.toString()}");
//       _token["count"] = dataResponse["count"];
//     }

//     return response.body;
//   }

//   Future<dynamic> addRow(String type, String idSensor) async {
//     var response;

//     if (_token["token"] == null || _token["count"] < 1) {
//       final loginResponse = await http.post('http://192.168.4.1/getLogin',
//           body: jsonEncode({"key": "1o2Ou"}));

//       print(loginResponse.body);
//       final loginData = json.decode(loginResponse.body);
//       _token["token"] = loginData["token"];
//       _token["count"] = loginData["count"];
//     }
//     if (type == "linier") {
//       response = await http.post(
//         'http://192.168.4.1/getData',
//         body: jsonEncode({"id": idSensor, "type": 0, "token": _token["token"]}),
//       );
//       final dataResponse = json.decode(response.body);

//       _token["count"] = dataResponse["count"];

//       if (dataResponse["value"] != null &&
//           double.tryParse(dataResponse["value"]) != null) {
//         _oldJson["a"] = double.parse(dataResponse["a"]) / 10000;
//         _newJson["a"] = null;
//         _value.add(
//           {
//             "rawValue": double.parse(dataResponse["value"]),
//             "id": DateTime.now().toIso8601String()
//           },
//         );
//       }
//     } else if (type == "non-linier") {
//       response = await http.post(
//         'http://192.168.4.1/getData',
//         body: jsonEncode({"id": idSensor, "type": 1, "token": _token["token"]}),
//       );
//       final dataResponse = json.decode(response.body);

//       _token["count"] = dataResponse["count"];

//       if (dataResponse["value"] != null &&
//           double.tryParse(dataResponse["value"]) != null) {
//         _oldJson["a"] = double.parse(dataResponse["a"]) / 10000;
//         _oldJson["b"] = double.parse(dataResponse["b"]) / 10000;
//         _newJson["a"] = null;
//         _newJson["b"] = null;
//         _value.add(
//           {
//             "rawValue": double.parse(dataResponse["value"]),
//             "id": DateTime.now().toIso8601String()
//           },
//         );
//       }

//       // response =
//       //     await http.get('http://192.168.4.1/getData/getData');
//       // final actualArray = json.decode(response.body)["actual"];
//       // final meanValue = actualArray.map((m) => m).reduce((a, b) => a + b) /
//       //     actualArray.length;
//       // _oldJson["a"] = json.decode(response.body)["a"];
//       // _oldJson["b"] = json.decode(response.body)["b"];
//       // _newJson["a"] = null;
//       // _newJson["b"] = null;
//       // _value.add(
//       //   {"rawValue": meanValue, "id": DateTime.now().toIso8601String()},
//       // );
//     } else if (type == "timer") {
//       _oldJson["timer"] = 300;
//       _oldJson["total-timer"] = 1000;
//       _oldJson["saturation"] = 50;

//       _newJson["power-factor"] =
//           100 * _oldJson["timer"] / _oldJson["total-timer"];
//     }

//     if (_value.length > 0) {
//       generateNewJson(type);
//     }

//     return response;
//   }

//   Widget getOldData(String type) {
//     List<Widget> rowItem = [];
//     for (var key in _oldJson.keys) {
//       rowItem.add(
//         Text(
//           "${key} : ${_oldJson[key]}",
//           style: TextStyle(
//             color: Colors.red,
//           ),
//         ),
//       );
//     }
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           (type != "timer")
//               ? Text(
//                   "Old Data :",
//                   style:
//                       TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
//                 )
//               : SizedBox(
//                   width: 0.0,
//                 ),
//           ...rowItem
//         ],
//       ),
//     );
//   }

//   Widget getNewData(String type) {
//     List<Widget> rowItem = [];
//     for (var key in _newJson.keys) {
//       rowItem.add(
//         Text(
//           "${key} : ${_newJson[key]}",
//           style: TextStyle(
//             color: Colors.green,
//           ),
//         ),
//       );
//     }
//     return Padding(
//       padding: const EdgeInsets.all(8.0),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           (type != "timer")
//               ? Text(
//                   "New Data :",
//                   style: TextStyle(
//                       color: Colors.green, fontWeight: FontWeight.bold),
//                 )
//               : SizedBox(
//                   width: 0.0,
//                 ),
//           ...rowItem
//         ],
//       ),
//     );
//   }

//   getVal() {
//     return [..._value];
//   }

//   getScore() {
//     var score = [];

//     for (var key in _oldJson.keys) {
//       score.add((double.parse(_newJson[key]) - _oldJson[key]));
//     }
//     return score.map((m) => m).reduce((a, b) => a + b) / score.length;
//   }

//   getSendData(String id) {
//     return {..._newJson, "idSensor": id};
//   }

//   deleteByValue(value, type) {
//     _value.remove(value);
//     generateNewJson(type);
//   }

//   updateValue(elemen, value, type) {
//     int index = _value.indexOf(elemen);
//     _value[index]["actualValue"] = double.parse(value);
//     generateNewJson(type);
//   }

//   clear() {
//     _value = [];
//     _oldJson = {};
//     _newJson = {};
//   }
// }
