import 'package:location/location.dart';
import 'dart:async';

class LocationWrapper {
  Location location = Location();
  StreamSubscription<LocationData> locationSubscription;

  void prepareLocationMonitoring() {

    location.changeSettings(
        interval: 5000,
        distanceFilter: 0,
      ).then((value){
        location.hasPermission().then((hasPermission) {
        if (hasPermission != PermissionStatus.granted) {
          location.requestPermission().then((permissionGranted) {
            if (permissionGranted == PermissionStatus.granted) {
              return true;
            }else{
              return false;
            }
          });
        } else {
          return true;
        }
      });
    });
  }

  void realtimedata(Function handler) {
    locationSubscription = location.onLocationChanged.listen((LocationData newLocation) {
      handler(newLocation);
    },);
  }

  void stopStream(){
    locationSubscription.cancel();
  }
  void resumeStream(){
    locationSubscription.resume();
  }
}