import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class EspUser {
  List _value = [];
  Map _oldParameter = {};
  Map _newParameter = {};
  double score = 0;
  int _typeData;
  Map<String, dynamic> _token = {"token": null, "count": 0};

  String url = "http://192.168.4.1/";
  String key = "1o2Ou";

  List<dynamic> _sampling = [];
  String samplingValue = "0";

  Future<void> checkToken() async {
    if (_token["token"] == null || _token["count"] < 1) {
      final loginResponse = await http.post(
        "${url}getLogin",
        body: jsonEncode(
          {"key": key},
        ),
      );
      final loginData = json.decode(loginResponse.body);
      _token["token"] = loginData["token"];
      _token["count"] = loginData["count"];
    }
  }

  Future<dynamic> getDataAll() async {
    final response = await http.get('${url}getData/all');
    final responseData = json.decode(response.body);
    return responseData["data"];
  }

  Future<List<List<String>>> getTransmitter() async {
    await checkToken();
    List<List<String>> dataTable;
    dataTable = [];
    final response = await http.get('${url}getData/other');
    final Map<String, dynamic> dataResponse = json.decode(response.body);
    _sampling = dataResponse["sampling"];
    for (var i = 0; i < dataResponse["sigfox"].length; i++) {
      final responseTransmitter = await http.post(
        '${url}getData',
        body: jsonEncode(
          {
            "id": dataResponse["sigfox"][i]["id"],
            "type": dataResponse["sigfox"][i]["type"],
            "token": _token["token"],
          },
        ),
      );
      final transmitterJson = json.decode(responseTransmitter.body);

      if (i % 2 == 0 && transmitterJson["value"] != "noCARD") {
        dataTable.add([
          "transmitter-${(dataTable.length + 1).toString()}",
          transmitterJson["value"].substring(1)
        ]);
      }
      updateToken(dataResponse);
    }

    await getSampling();
    return dataTable;
  }

  Future<String> getSampling() async {
    final read_sampling =
        _sampling.firstWhere((element) => element["name"].contains("read"));

    final responSampling = await http.post(
      '${url}getData',
      body: jsonEncode(
        {
          "id": read_sampling["id"],
          "type": read_sampling["type"],
          "token": _token["token"],
        },
      ),
    );
    final transmitterJson = json.decode(responSampling.body);
    print(transmitterJson);
    samplingValue = transmitterJson["value"].toString();
    updateToken(transmitterJson);
    return samplingValue;
  }

  Future<bool> setSampling(int value) async {
    final writeSampling =
        _sampling.firstWhere((element) => element["name"].contains("write"));

    Map body = {
      "id": writeSampling["id"],
      "token": _token["token"],
      "a": sampling2string(value.toString()),
      "b": "00000",
    };

    await checkToken();
    http.Response response = await http.post(
      '${url}setData',
      body: jsonEncode(
        body,
      ),
    );
    final dataResponse = json.decode(response.body);
    print(dataResponse);
    updateToken(dataResponse);

    return true;
  }

  Future<bool> shutDown() async {
    final response =
        await http.post('${url}shutdown', body: jsonEncode({"key": key}));
    return true;
  }

  Future<Map> getSingleSensor(int type, String idSensor) async {
    await checkToken();
    //
    http.Response response;
    response = await http.post(
      '${url}getData',
      body: jsonEncode(
        {
          "id": idSensor,
          "type": type,
          "token": _token["token"],
        },
      ),
    );
    final dataResponse = json.decode(response.body);
    updateToken(dataResponse);
    if (dataResponse["value"] != null &&
        double.tryParse(dataResponse["value"]) != null &&
        dataResponse["a"] != null &&
        dataResponse["b"] != null) {
      return dataResponse;
    } else {
      return null;
    }
  }

  Future<dynamic> getDataSensor(int type, String idSensor) async {
    await checkToken();
    //
    http.Response response;
    response = await http.post(
      '${url}getData',
      body: jsonEncode(
        {
          "id": idSensor,
          "type": type,
          "token": _token["token"],
        },
      ),
    );
    //
    final dataResponse = json.decode(response.body);

    updateToken(dataResponse);
    // reset parameter
    if (dataResponse["value"] != null &&
        double.tryParse(dataResponse["value"]) != null &&
        dataResponse["a"] != null &&
        dataResponse["b"] != null) {
      if (type == 0) {
        _oldParameter["a"] = double.parse(dataResponse["a"]) / 10000;
        _newParameter["a"] = null;
        _typeData = 0;
      } else if (type == 1) {
        _oldParameter["a"] = double.parse(dataResponse["a"]) / 10000;
        _oldParameter["b"] = double.parse(dataResponse["b"]);
        _newParameter["a"] = null;
        _newParameter["b"] = null;
        _typeData = 1;
      } else if (type == 2) {
        _oldParameter["a"] = double.parse(dataResponse["a"]);
        _newParameter["a"] = null;
        _typeData = 2;
      }

      _value.add(
        {
          "rawValue": double.parse(dataResponse["value"]),
          "id": DateTime.now().toIso8601String()
        },
      );
      generateNewParameter();
    }

    return response.body;
  }

  Future<dynamic> setDataSensor(String id) async {
    await checkToken();
    Map body;
    if (_typeData == 2) {
      body = {
        "id": id,
        "token": _token["token"],
        "a": gain2string(_value[0]["actualValue"].toString()),
        "b": "00000",
        "value": _value[0]["actualValue"],
      };
    } else {
      body = {
        "id": id,
        "token": _token["token"],
        "a": gain2string(_newParameter["a"]),
        "b": _typeData == 1 ? gain2stringB(_newParameter["b"]) : "00000",
      };
    }
    print(body);
    http.Response response = await http.post(
      '${url}setData',
      body: jsonEncode(
        body,
      ),
    );

    print(response.body);
    final dataResponse = json.decode(response.body);
    updateToken(dataResponse);

    return 1;
  }

  Future<dynamic> sendOtp(String otp) async {
    Map body = {"token": _token["token"], "message": otp};
    print(body);
    await checkToken();
    http.Response response = await http.post(
      '${url}write-OTP',
      body: jsonEncode(
        body,
      ),
    );
    final dataResponse = json.decode(response.body);
    updateToken(dataResponse);
    return dataResponse;
  }

  Future<dynamic> testSigfox(String idTransmitter) async {
    Map body = {"token": _token["token"], "id": idTransmitter};
    await checkToken();
    http.Response response = await http.post(
      '${url}testSigfox',
      body: jsonEncode(
        body,
      ),
    );
    final dataResponse = json.decode(response.body);
    updateToken(dataResponse);
    return dataResponse;
  }

  Future<dynamic> quickSend() async {
    Map body = {"key": key};
    print(body);
    await checkToken();
    http.Response response = await http.post(
      '${url}sendSigfox',
      body: jsonEncode(
        body,
      ),
    );
    final dataResponse = json.decode(response.body);
    print(dataResponse);
    updateToken(dataResponse);
    return dataResponse;
  }

  void updateToken(dataResponse) {
    _token["count"] = dataResponse["count"];
  }

  String randomString(int length) {
    var codeUnits = new List.generate(length, (index) {
      return 0;
    });
    return new String.fromCharCodes(codeUnits);
  }

  String sampling2string(String gain) {
    final stringGain = gain.toString();
    String gainSixDigit;

    if (stringGain.length < 5) {
      var codeUnits = new List.generate(5 - stringGain.length, (index) {
        return 0;
      });
      var concatenate = StringBuffer();

      codeUnits.forEach((item) {
        concatenate.write(item);
      });

      gainSixDigit = "${concatenate}$stringGain";
    } else {
      gainSixDigit = stringGain.substring(0, 5);
    }
    return gainSixDigit;
  }

  String gain2string(String gain) {
    final stringGain = (double.parse(gain) * 10000).toString();
    String gainSixDigit;
    if (stringGain.length < 5) {
      final zeroString = randomString(5 - stringGain.length);
      gainSixDigit = "$zeroString$stringGain";
    } else {
      gainSixDigit = stringGain.substring(0, 5);
    }
    return gainSixDigit;
  }

  String gain2stringB(String gain) {
    final stringGain = (double.parse(gain)).toString();
    String gainSixDigit;
    if (stringGain.length < 5) {
      final zeroString = randomString(5 - stringGain.length);
      gainSixDigit = "$zeroString$stringGain";
    } else {
      gainSixDigit = stringGain.substring(0, 5);
    }
    return gainSixDigit;
  }

  void generateNewParameter() {
    if (_value.length > 0) {
      if (_typeData == 0) {
        double meanSensorValue =
            _value.map((m) => m["rawValue"]).reduce((a, b) => a + b) /
                _value.length;
        double meanActualValue = _value
                .map((m) => m["actualValue"] == null ? 0 : m["actualValue"])
                .reduce((a, b) => a + b) /
            _value.length;
        _newParameter["a"] =
            (meanActualValue / meanSensorValue).toStringAsFixed(4);
      } else if (_typeData == 1) {
        var meanSensorValue = _value.map<double>((m) => m["rawValue"]).toList();
        var meanActualValue =
            _value.map<double>((m) => m["actualValue"]).toList();
        if (meanActualValue.contains(null)) {
        } else {
          if (meanActualValue.length == 2 && meanSensorValue.length == 2) {
            var a = (meanActualValue[0] - meanActualValue[1]) /
                (meanSensorValue[0] - meanSensorValue[1]);
            var b = meanActualValue[0] - (a * meanSensorValue[0]);
            _newParameter["a"] = a.toStringAsFixed(4);
            _newParameter["b"] = b.abs().toStringAsFixed(4);
          }
        }
      } else if (_typeData == 1) {
        double meanSensorValue =
            _value.map((m) => m["rawValue"]).reduce((a, b) => a + b) /
                _value.length;
        double meanActualValue = _value
                .map((m) => m["actualValue"] == null ? 0 : m["actualValue"])
                .reduce((a, b) => a + b) /
            _value.length;
        _newParameter["a"] =
            (meanActualValue / meanSensorValue).toStringAsFixed(4);
      } else if (_typeData == 2) {
        _newParameter["sampling"] = _value[0]["actualValue"];
      }
    }
  }

  Widget getOldParam() {
    List<Widget> rowItem = [];
    for (var key in _oldParameter.keys) {
      rowItem.add(
        Text(
          "${_typeData == '2' ? 'value' : key} : ${_oldParameter[key]}",
          style: TextStyle(
            color: Colors.red,
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Old Data :",
            style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
          ),
          ...rowItem
        ],
      ),
    );
  }

  Widget getNewParam() {
    List<Widget> rowItem = [];
    for (var key in _newParameter.keys) {
      rowItem.add(
        Text(
          "${_typeData == '2' ? 'value' : key} : ${_newParameter[key]}",
          style: TextStyle(
            color: Colors.green,
          ),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "New Data :",
            style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
          ),
          ...rowItem
        ],
      ),
    );
  }

  getVal() {
    return [..._value];
  }

  getScore() {
    var score = [];

    if (_typeData == 2) {
      return 0.0;
    }
    for (var key in _newParameter.keys) {
      score.add((double.parse(_newParameter[key]) - _oldParameter[key]));
    }
    return score.map((m) => m).reduce((a, b) => a + b) / score.length;
  }

  getSendData(String id) {
    return {..._newParameter, "idSensor": id};
  }

  deleteByValue(value) {
    _value.remove(value);
    generateNewParameter();
  }

  updateValue(elemen, value) {
    int index = _value.indexOf(elemen);
    _value[index]["actualValue"] = double.parse(value);
    generateNewParameter();
  }

  clear() {
    _value = [];
    _oldParameter = {};
    _newParameter = {};
    score = 0;
    _typeData = null;
  }
}
