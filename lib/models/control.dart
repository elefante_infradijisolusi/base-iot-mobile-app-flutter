import 'package:flutter/foundation.dart';

class Control {
  final String id;
  final String name;
  final ControlType type;
  final double min;
  final double max;
  final double step;
  final double value;
  final String ip;
  final String port;
  final String topic;

  Control({
    @required this.id,
    @required this.name,
    @required this.type,
    @required this.min,
    @required this.max,
    @required this.step,
    @required this.ip,
    @required this.port,
    @required this.topic,
    @required this.value,
  });
}

enum ControlType {
  onof,
  slider,
  value,
}
