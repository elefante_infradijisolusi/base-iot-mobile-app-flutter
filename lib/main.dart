import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/tabs_screen.dart';
import './screens/detail_node_screen.dart';
import './screens/auth_screen.dart';
import './screens/splash_screen.dart';

import './providers/controls.dart';
import './providers/auth.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Auth(),
        ),
        ChangeNotifierProvider(create: (_) => Controls()),
      ], // void clickItem(ctx, arg) {
      //   Navigator.of(ctx).pushNamed(DetailNode.routeName, arguments: arg);

      // }
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Consumer<Auth>(
      builder: (ctx, auth, _) => MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primaryColor: Color(0xFF424C58),
          scaffoldBackgroundColor: Colors.grey[80],
          appBarTheme: AppBarTheme(
              color: Color(0xFF424C58),
              iconTheme: IconThemeData(color: Colors.white),
              actionsIconTheme: IconThemeData(color: Colors.white)),
          accentColor: Color(0xFF1CC7DE),
          canvasColor: Colors.white,
          iconTheme: IconThemeData.fallback().copyWith(
            color: Colors.white,
          ),
          inputDecorationTheme: InputDecorationTheme(
            border: OutlineInputBorder(),
          ),
          textTheme: ThemeData.light().textTheme.copyWith(
                bodyText1: TextStyle(
                  color: Colors.black,
                ),
                bodyText2: TextStyle(
                  color: Colors.white,
                ),
                headline1: TextStyle(color: Colors.white),
              ),
        ),
        home: auth.isAuth
            ? TabScreen()
            : FutureBuilder(
                future: auth.tryAutoLogin(),
                builder: (ctx, authResultSnapshot) =>
                    authResultSnapshot.connectionState ==
                            ConnectionState.waiting
                        ? SplashScreen()
                        : AuthScreen(),
              ),
        routes: {
          TabScreen.routeName: (ctx) => TabScreen(),
          // DetailNode.routeName: (ctx) => DetailNode()
        },
      ),
    );
  }
}
