import 'package:flutter/material.dart';
import 'package:ff_navigation_bar/ff_navigation_bar.dart';
import 'package:flushbar/flushbar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'dart:io' show Platform;

import './map_screen.dart';
import './chart_screen.dart';
import './control_screen.dart';
import './user_screen.dart';
import './qr_screen.dart';
import './account_screen.dart';

class TabScreen extends StatefulWidget {
  static const routeName = '/tab';

  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  Flushbar flush;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  final List<Widget> _pages = [
    Text('null'),
    ChartScreen(),
    QrScreen(),
    AccountScreen(),
  ];

  int _selectedPageIndex = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseCloudMessaging_Listeners();
  }

  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    _firebaseMessaging.getToken().then((token) {
      print(token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        ShowFlushBar(message["notification"]["title"],
            Text(message["notification"]["body"]),
            status: "danger");
      },
      onResume: (Map<String, dynamic> message) async {
        ShowFlushBar(message["notification"]["title"],
            Text(message["notification"]["body"]),
            status: "danger");
      },
      onLaunch: (Map<String, dynamic> message) async {
        ShowFlushBar(message["notification"]["title"],
            Text(message["notification"]["body"]),
            status: "danger");
      },
    );

    _firebaseMessaging.subscribeToTopic('iot');
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void ShowFlushBar(String title, Widget msg, {String status = "normal"}) {
    print(title);
    if (flush != null) {
      if (flush.isShowing()) {
        flush.dismiss();
      }
    }
    flush = Flushbar(
      animationDuration: Duration(milliseconds: 500),
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      icon: status == "normal"
          ? null
          : Icon(
              Icons.warning,
              color: Colors.red,
            ),
      shouldIconPulse: status == "normal" ? false : true,
      title: title,
      messageText: msg,
      // mainButton: FlatButton(
      //   onPressed: () {
      //     flush.dismiss();
      //   },
      //   child: Text(
      //     "Close",
      //     style: TextStyle(color: Colors.red),
      //   ),
      // ),
      // shouldIconPulse: true,
      leftBarIndicatorColor:
          status == "normal" ? Theme.of(context).accentColor : Colors.red,
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: _appBar[_selectedPageIndex],
      body: _selectedPageIndex == 0
          ? MapScreen(ShowFlushBar)
          : _pages[_selectedPageIndex],
      bottomNavigationBar: FFNavigationBar(
        theme: FFNavigationBarTheme(
          // barBackgroundColor: Colors.white,
          selectedItemBorderColor: Colors.transparent,
          selectedItemBackgroundColor: Theme.of(context).appBarTheme.color,
          selectedItemIconColor: Theme.of(context).textTheme.headline1.color,
          selectedItemLabelColor: Theme.of(context).appBarTheme.color,
          showSelectedItemShadow: false,
          barHeight: 70,
        ),
        selectedIndex: _selectedPageIndex,
        onSelectTab: (index) {
          setState(() {
            _selectedPageIndex = index;
          });
        },
        items: [
          FFNavigationBarItem(
            iconData: MdiIcons.mapCheck,
            label: 'Maps',
          ),
          FFNavigationBarItem(
            iconData: MdiIcons.chip,
            label: 'Device',
            // selectedBackgroundColor: Colors.orange,
          ),
          FFNavigationBarItem(
            iconData: Icons.settings_applications,
            label: 'Config',
            // selectedBackgroundColor: Colors.blue,
          ),
          FFNavigationBarItem(
            iconData: Icons.account_circle,
            label: 'User',
            // selectedBackgroundColor: Colors.blue,
          ),
        ],
      ),
    );
  }
}
