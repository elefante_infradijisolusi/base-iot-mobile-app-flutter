import 'dart:ffi';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:location/location.dart';
import 'package:flushbar/flushbar.dart';

class SettingNode extends StatefulWidget {
  final String id;
  const SettingNode({this.id});

  @override
  _SettingNodeState createState() => _SettingNodeState();
}

class _SettingNodeState extends State<SettingNode> {
  MapboxMapController controller;
  Symbol _markerSymbol;
  LatLng center = LatLng(-7.265029807528294, 112.78100967407225);
  double zoom = 13.0;
  bool isSwitched = false;

  final nameController = TextEditingController();
  final latController = TextEditingController();
  final lngController = TextEditingController();
  final katController = TextEditingController();

  @override
  void dispose() {
    nameController.dispose();
    latController.dispose();
    lngController.dispose();
    katController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void changeMarker(double lat, double lng, String name) {}

  void _getLocation() async {
    var location = new Location();
    try {
      await location.getLocation().then((onValue) {
        latController.text = onValue.latitude.toString();
        lngController.text = onValue.longitude.toString();

        controller.updateSymbol(
            _markerSymbol,
            SymbolOptions(
              draggable: true,
              geometry: LatLng(onValue.latitude, onValue.longitude),
              iconImage: 'marker_symbol',
              iconAnchor: 'bottom',
              textField: nameController.text,
              textSize: 15,
              iconSize: 1.5,
              textOffset: Offset(0, -3.8),
            ));
        controller.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              bearing: 0.0,
              tilt: 0.0,
              zoom: zoom,
              target: LatLng(onValue.latitude, onValue.longitude),
            ),
          ),
        );
      });
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {}
    }
  }

  void _onMapCreated(MapboxMapController controller) {
    this.controller = controller;
    _fechData();
  }

  void _onStyleLoaded() {
    addImageFromAsset("marker_symbol", "assets/symbol/marker.png");
  }

  /// Adds an asset image to the currently displayed style
  Future<void> addImageFromAsset(String name, String assetName) async {
    final ByteData bytes = await rootBundle.load(assetName);
    final Uint8List list = bytes.buffer.asUint8List();
    return controller.addImage(name, list);
  }

  Future<Void> _fechData() async {
    try {
      final response =
          await http.get(widget.id, headers: {"Accept": "application/json"});
      Map<String, dynamic> dataJson = jsonDecode(response.body);

      nameController.text = dataJson["name"];
      katController.text = dataJson["category"];
      latController.text = dataJson["location"]["lat"].toString();
      lngController.text = dataJson["location"]["long"].toString();

      controller
          .addSymbol(
        SymbolOptions(
          draggable: true,
          geometry: LatLng(double.tryParse(latController.text),
              double.tryParse(lngController.text)),
          iconImage: 'marker_symbol',
          iconAnchor: 'bottom',
          textField: nameController.text,
          textSize: 15,
          iconSize: 1.5,
          textOffset: Offset(0, -3.8),
        ),
      )
          .then((value) {
        _markerSymbol = value;
      });
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            bearing: 0.0,
            tilt: 0.0,
            zoom: zoom,
            target: LatLng(double.tryParse(latController.text),
                double.tryParse(lngController.text)),
          ),
        ),
      );
    } catch (_) {
      print('error');
    }
  }

  Future<Void> _postData() async {
    try {
      final response =
          await http.get(widget.id, headers: {"Accept": "application/json"});
    } catch (_) {
      print('error');
    }
  }

  @override
  void initState() {
    // chartSlider = createLineWidget();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // nameController.text = widget.id;
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: const Text(
          "Details",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              flex: 4,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 2,
                  child: MapboxMap(
                    onMapCreated: _onMapCreated,
                    onStyleLoadedCallback: _onStyleLoaded,
                    initialCameraPosition: CameraPosition(
                      bearing: 0.0,
                      tilt: 0.0,
                      zoom: zoom,
                      target: center,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: IconButton(
                          onPressed: () {}, icon: const Icon(Icons.device_hub)),
                      title: TextField(
                        // initialValue: widget.id,
                        controller: nameController,
                        decoration: InputDecoration(
                          hintText: "Device Name",
                        ),
                      ),
                    ),
                    ListTile(
                      leading: IconButton(
                          onPressed: () {}, icon: const Icon(Icons.category)),
                      title: TextFormField(
                        controller: katController,
                        decoration: InputDecoration(
                          hintText: "Device Category",
                        ),
                      ),
                    ),
                    ListTile(
                      leading: IconButton(
                        onPressed: () {
                          if (!isSwitched) {
                            _getLocation();
                          }

                          setState(() {
                            isSwitched = !isSwitched;
                          });
                        },
                        padding: EdgeInsets.all(0),
                        icon: Icon(
                          Icons.my_location,
                          color: isSwitched
                              ? Theme.of(context).accentColor
                              : Colors.grey[550],
                        ),
                      ),
                      title: Row(
                        children: [
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: latController,
                              decoration: InputDecoration(
                                hintText: "Latitude",
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.number,
                              controller: lngController,
                              decoration: InputDecoration(
                                hintText: "Longitude",
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
                child: Row(
                  children: [
                    Expanded(
                      child: RaisedButton.icon(
                        color: Theme.of(context).appBarTheme.color,
                        onPressed: () {
                          controller.updateSymbol(
                              _markerSymbol,
                              SymbolOptions(
                                draggable: true,
                                geometry: LatLng(
                                  double.tryParse(latController.text),
                                  double.tryParse(lngController.text),
                                ),
                                iconImage: 'marker_symbol',
                                iconAnchor: 'bottom',
                                textField: nameController.text,
                                textSize: 15,
                                iconSize: 1.5,
                                textOffset: Offset(0, -3.8),
                              ));
                          controller.animateCamera(
                            CameraUpdate.newCameraPosition(
                              CameraPosition(
                                bearing: 0.0,
                                tilt: 0.0,
                                zoom: zoom,
                                target: LatLng(
                                  double.tryParse(latController.text),
                                  double.tryParse(lngController.text),
                                ),
                              ),
                            ),
                          );
                        },
                        icon: Icon(
                          Icons.remove_red_eye,
                          color: Colors.white,
                        ),
                        label: Text(
                          "Preview",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: RaisedButton.icon(
                        color: Theme.of(context).appBarTheme.color,
                        onPressed: () {
                          Flushbar(
                            animationDuration: Duration(milliseconds: 500),
                            dismissDirection:
                                FlushbarDismissDirection.HORIZONTAL,
                            flushbarPosition: FlushbarPosition.TOP,
                            padding: EdgeInsets.all(20),
                            flushbarStyle: FlushbarStyle.FLOATING,
                            message: "Save Data",
                            duration: Duration(seconds: 1),
                            icon: Icon(
                              Icons.check,
                              color: Theme.of(context).accentColor,
                            ),
                            shouldIconPulse: false,
                            leftBarIndicatorColor:
                                Theme.of(context).accentColor,
                          )..show(context);
                        },
                        icon: Icon(
                          Icons.save,
                          color: Colors.white,
                        ),
                        label: Text(
                          "Save",
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
