import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import '../providers/auth.dart';

import '../models/task.dart';
import '../helper/text_helper.dart';

import '../screens/task_screen.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  ListTask listTask = ListTask();
  DateFormat dateFormat = DateFormat("dd/MM/yyyy");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<Auth>(context, listen: false).getTask().then((value) {
      setState(() {
        listTask.createTask(value["task"]);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppBar(
          brightness: Brightness.dark,
          title: Text(
            "Task",
            style: TextStyle(color: Colors.white),
          ),
          actions: [
            FlatButton.icon(
              onPressed: () {
                Provider.of<Auth>(context, listen: false).logout();
              },
              icon: Icon(
                Icons.exit_to_app,
                color: Colors.red,
              ),
              label: Text(
                "Logout",
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            )
          ],
        ),
        Expanded(
          child: ListView.builder(
            itemCount: listTask.getLength(),
            itemBuilder: (context, index) {
              final Task task = listTask.getTask(index);
              return Card(
                child: ListTile(
                  // leading: Text("${(index + 1).toString()}"),
                  title: Text(
                    "${task.title.capitalize()}",
                  ),
                  subtitle: Row(
                    children: [
                      Text(
                        // "${dateFormat.format(task.startDate)} - ${dateFormat.format(task.endDate)}",
                        "${dateFormat.format(task.endDate)}",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "(${task.endDate.difference(DateTime.now()).inDays} days left)",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      )
                    ],
                  ),
                  trailing: Text(
                    task.priority.toUpperCase(),
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TaskScreen(
                          task: task,
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
