import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:number_inc_dec/number_inc_dec.dart';
import 'package:location/location.dart';

import '../models/espUser.dart';

import '../providers/auth.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatefulWidget {
  final String id;
  const SettingScreen({this.id});

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final EspUser espController = EspUser();
  bool _off = false;
  bool _load = false;
  List<List<String>> dataTable = [];
  Map<String, dynamic> _dataDevice = {
    'device-name': '-',
    "device-category": "-",
    "transmiter": json.encode([]),
    "sensors": json.encode(
      {
        "voltage": {
          "min": 0,
          "tmin": 180,
          "tmax": 240,
          "max": 280,
        },
        "current": {
          "min": 0,
          "tmin": 100,
          "tmax": 120,
          "max": 150,
        },
        "temperature": {
          "min": 40,
          "tmin": 60,
          "tmax": 80,
          "max": 120,
        }
      },
    ),
  };
  TextEditingController myController = TextEditingController();
  LocationData currentLocation;

  void _getLocation() async {
    var location = new Location();
    try {
      await location.getLocation().then((onValue) {
        setState(() {
          currentLocation = onValue;
        });
      });
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {}
    }
  }

  void fetchSensorList() async {
    setState(() {
      _load = true;
    });
    final responseTransmitter = await espController.getTransmitter();
    final responseSampling = await espController.getSampling();
    setState(() {
      dataTable = responseTransmitter;
      myController.text = espController.samplingValue;
      _load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // fetchSensorList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: const Text(
          "Setting",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    color: Colors.white,
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Text(
                              "Data Device",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        _load
                            ? Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: CircularProgressIndicator(),
                              )
                            : Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: Table(
                                  border: TableBorder(
                                    horizontalInside: BorderSide(
                                      width: 1,
                                      color: Colors.grey,
                                      style: BorderStyle.solid,
                                    ),
                                    top: BorderSide(
                                      width: 1,
                                      color: Colors.grey,
                                      style: BorderStyle.solid,
                                    ),
                                    bottom: BorderSide(
                                      width: 1,
                                      color: Colors.grey,
                                      style: BorderStyle.solid,
                                    ),
                                    left: BorderSide(
                                      width: 1,
                                      color: Colors.grey,
                                      style: BorderStyle.solid,
                                    ),
                                    right: BorderSide(
                                      width: 1,
                                      color: Colors.grey,
                                      style: BorderStyle.solid,
                                    ),
                                  ),
                                  children: [
                                    TableRow(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Transmitter',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'ID',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontWeight: FontWeight.bold),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                    ...dataTable
                                        .map<TableRow>(
                                          (element) => TableRow(
                                            children: [
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(5.0),
                                                child: Text(
                                                  element[0],
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(5.0),
                                                child: Text(
                                                  element[1],
                                                  style: TextStyle(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                        .toList(),
                                  ],
                                ),
                              ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 6.0),
                            child: Text(
                              "Name",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            ),
                            onSaved: (value) {
                              _dataDevice['device-name'] = value;
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 6.0),
                            child: Text(
                              "Location",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(15.0),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                  border: Border.all(
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Text(
                                  "Lat : ${currentLocation == null ? '-' : currentLocation.latitude.toStringAsFixed(5)}",
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(15.0),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0)),
                                  border: Border.all(
                                    color: Colors.grey,
                                  ),
                                ),
                                child: Text(
                                  "Long : ${currentLocation == null ? '-' : currentLocation.longitude.toStringAsFixed(5)}",
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.my_location),
                                color: currentLocation == null
                                    ? Colors.grey
                                    : Theme.of(context).accentColor,
                                onPressed: () {
                                  _getLocation();
                                },
                              )
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 6.0),
                            child: Text(
                              "Sampling (Minutes)",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: NumberInputWithIncrementDecrement(
                            controller: myController,
                            min: 5,
                            max: 30,
                            incIconColor: Theme.of(context).accentColor,
                            decIconColor: Theme.of(context).accentColor,
                            incIconSize: 30,
                            decIconSize: 30,
                            incIconDecoration: BoxDecoration(border: null),
                            widgetContainerDecoration:
                                BoxDecoration(border: null),
                            onIncrement: (newValue) {
                              espController.setSampling(newValue);
                            },
                            onDecrement: (newValue) {
                              espController.setSampling(newValue);
                            },
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 6.0),
                            child: Text(
                              "Route",
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                        ...[
                          {"index": 0, "name": "Jalur-1"},
                          {"index": 1, "name": "Jalur-2"},
                          {"index": 2, "name": "Jalur-3"},
                        ].map((Map<String, dynamic> e) {
                          return Padding(
                            padding: const EdgeInsets.only(
                              top: 8.0,
                              left: 10.0,
                            ),
                            child: TextFormField(
                              initialValue: e["name"],
                              decoration: InputDecoration(
                                labelText: (e["index"] + 1).toString(),
                                contentPadding:
                                    EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                              ),
                              onSaved: (value) {},
                            ),
                          );
                        }).toList(),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0, top: 30),
                          child: Row(
                            // crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Expanded(
                                flex: 3,
                                child: FlatButton.icon(
                                  onPressed: () => {},
                                  height: 45,
                                  icon: Icon(
                                    Icons.email,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    "Quick Send",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 3,
                                child: FlatButton.icon(
                                  color: Theme.of(context).accentColor,
                                  onPressed: () {
                                    _formKey.currentState.save();

                                    if (currentLocation != null) {
                                      _dataDevice["position"] = json.encode({
                                        "latitude": currentLocation.latitude,
                                        "longitude": currentLocation.longitude,
                                      });
                                    }
                                    _dataDevice["module-id"] = widget.id;
                                    _dataDevice["transmiter"] = jsonEncode(
                                      dataTable
                                          .map((e) => e[1].toLowerCase())
                                          .toList(),
                                    );

                                    var rndnumber = "";
                                    var rnd = new Random();
                                    for (var i = 0; i < 20; i++) {
                                      rndnumber =
                                          rndnumber + rnd.nextInt(9).toString();
                                    }

                                    espController.sendOtp(rndnumber.toString());
                                    espController.testSigfox("T");
                                    espController.quickSend();

                                    Provider.of<Auth>(context, listen: false)
                                        .postDevice(_dataDevice)
                                        .then((value) {});
                                  },
                                  height: 45,
                                  icon: Icon(
                                    Icons.send,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    "Submit",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: FlatButton.icon(
                            onPressed: () => {
                              espController.shutDown().then(
                                    (value) => print(value),
                                  )
                            },
                            height: 50,
                            minWidth: double.infinity,
                            icon: Icon(
                              Icons.power_settings_new,
                              color: Colors.white,
                            ),
                            label: Text(
                              "Shut Down",
                              style: TextStyle(color: Colors.white),
                            ),
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
