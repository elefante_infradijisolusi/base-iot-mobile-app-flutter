import 'dart:ffi';

import 'package:flutter/material.dart';
import '../models/espUser.dart';

import '../screens/data_sensor_screen.dart';
import '../widgets/icon_spinner.dart';

class SensorItem {
  final String id;
  final int index;
  final String name;
  double value;
  final int type;
  bool _isloading = false;
  Map _currentData = {};
  String unit = "";

  SensorItem({this.id, this.index, this.name, this.value, this.type});

  void updateInitialData(val) {
    _currentData = val;
  }

  void updateValue(double val) {
    value = val;
  }

  void updateLoadingState(bool val) {
    _isloading = val;
  }

  String getUnit() {
    if (name.toLowerCase().contains("voltage")) {
      return "V";
    } else if (name.toLowerCase().contains("current")) {
      return "A";
    } else if (name.toLowerCase().contains("temperature")) {
      return " \u00b0C";
    } else {
      return "";
    }
  }

  Widget getListWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: ListTile(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return SensorScreen(
                      id: id,
                      name: name,
                      type: type,
                      initialData: _currentData,
                      unit: getUnit(),
                    );
                  },
                ),
              );
            },
            title: Text(
              name,
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            leading: CircleAvatar(
              backgroundColor: Color(0xFF424C58),
              child: Text(
                "${(index + 1).toString()}",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            trailing: Text(
              "${value.toStringAsFixed(2)} ${getUnit()}",
              style: TextStyle(
                color: _isloading ? Colors.red : Colors.black,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class DataScreen extends StatefulWidget {
  final String id;
  const DataScreen({this.id});

  @override
  _DataScreenState createState() => _DataScreenState();
}

class _DataScreenState extends State<DataScreen> {
  EspUser espController = EspUser();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();
  List<SensorItem> dataSensor = [];
  bool _canload = false;

  void fetchSensorList() async {
    final response = await espController.getDataAll();

    setState(
      () {
        for (var i = 0; i < response.length; i++) {
          dataSensor.add(
            SensorItem(
              id: response[i]["id"],
              index: i,
              name: response[i]["name"],
              type: response[i]["type"],
              value: -1.0,
            ),
          );
        }
        dataSensor.length > 0 ? _canload = true : _canload = false;
      },
    );
  }

  Future<int> updateValue() async {
    for (var i = 0; i < dataSensor.length; i++) {
      if (mounted) {
        setState(() {
          dataSensor[i].updateLoadingState(true);
        });

        final data = await espController.getSingleSensor(
            dataSensor[i].type, dataSensor[i].id);
        dataSensor[i].updateInitialData(data);
        if (data != null) {
          setState(
            () {
              if (dataSensor[i].type == 0) {
                if (data["value"] == null) {
                  dataSensor[i].updateValue(-1);
                } else {
                  if (double.tryParse(data["value"]) == null) {
                    dataSensor[i].updateValue(-1);
                  } else {
                    final double val = double.parse(data["value"]) *
                        double.parse(data["a"]) /
                        10000;
                    dataSensor[i].updateValue(val);
                  }
                }
                dataSensor[i].updateLoadingState(false);
              } else if (dataSensor[i].type == 1) {
                if (data["value"] == null) {
                  dataSensor[i].updateValue(-1);
                } else {
                  if (double.tryParse(data["value"]) == null) {
                    dataSensor[i].updateValue(-1);
                  } else {
                    double val;
                    if (dataSensor[i]
                        .name
                        .toLowerCase()
                        .contains("temperature")) {
                      val = double.parse(data["value"]);
                    } else {
                      val = (double.parse(data["value"]) *
                                  double.parse(data["a"]) +
                              double.parse(data["b"])) /
                          10000;
                    }

                    dataSensor[i].updateValue(val);
                  }
                }
                dataSensor[i].updateLoadingState(false);
              } else if (dataSensor[i].type == 2) {
                if (data["value"] == null) {
                  dataSensor[i].updateValue(-1);
                } else {
                  if (double.tryParse(data["value"]) == null) {
                    dataSensor[i].updateValue(-1);
                  } else {
                    final double val = double.parse(data["value"]);
                    dataSensor[i].updateValue(val);
                  }
                }
                dataSensor[i].updateLoadingState(false);
              }
            },
          );
        } else {
          setState(() {
            dataSensor[i].updateValue(-1);
            dataSensor[i].updateLoadingState(false);
          });
        }
      }
    }
    return 1;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchSensorList();
  }

  Future<bool> _refresh() async {
    if (_canload) {
      setState(
        () {
          _canload = false;
        },
      );
      updateValue().then(
        (value) {
          setState(() {
            _canload = true;
          });
        },
      );
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: const Text(
          "Data Sensor",
          style: TextStyle(color: Colors.white),
        ),
        actions: <Widget>[
          IconButton(
              icon: _canload
                  ? Spinner(
                      icon: Icons.refresh,
                      color: Theme.of(context).accentColor,
                    )
                  : Icon(
                      Icons.refresh,
                    ),
              tooltip: 'Refresh',
              onPressed: () {
                _refreshIndicatorKey.currentState.show();
              }),
        ],
      ),
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: ListView.builder(
          itemCount: dataSensor.length,
          itemBuilder: (context, index) =>
              dataSensor[index].getListWidget(context),
        ),
      ),
    );
  }
}
