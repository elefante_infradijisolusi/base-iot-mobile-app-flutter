import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../screens/device_setting_screen.dart';
import '../screens/callibration_screen.dart';
import '../screens/checkup_screen.dart';
import '../screens/setting_screen.dart';
import '../screens/data_screen.dart';
import '../screens/status_sensor_screen.dart';

class QrScreen extends StatefulWidget {
  @override
  _QrScreenState createState() => _QrScreenState();
}

class _QrScreenState extends State<QrScreen> {
  String _scanBarcode = '-1';
  final GlobalKey<FormState> _formKey = GlobalKey();

  startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
            "#ff6666", "Cancel", true, ScanMode.BARCODE)
        .listen(
      (barcode) => print(barcode),
    );
  }

  Future<String> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.

    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#FF1CC7DE", "Cancel", true, ScanMode.QR);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return _scanBarcode;
    setState(() {
      _scanBarcode = barcodeScanRes;
    });

    return _scanBarcode;
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  int _radioValue = 0;
  String _deviceId;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          break;

        case 1:
          break;
      }
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text(
          "Maintenance",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 30),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Column(
                                  children: [
                                    Radio(
                                      value: 0,
                                      groupValue: _radioValue,
                                      onChanged: _handleRadioValueChange,
                                    ),
                                    Text(
                                      'Callibration',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Radio(
                                      value: 1,
                                      groupValue: _radioValue,
                                      onChanged: _handleRadioValueChange,
                                    ),
                                    Text(
                                      'Data Sensor',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Radio(
                                      value: 2,
                                      groupValue: _radioValue,
                                      onChanged: _handleRadioValueChange,
                                    ),
                                    Text(
                                      'Status Sensor',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Radio(
                                      value: 3,
                                      groupValue: _radioValue,
                                      onChanged: _handleRadioValueChange,
                                    ),
                                    Text(
                                      'Setting',
                                      style: TextStyle(
                                        fontSize: 10.0,
                                        color: Theme.of(context).primaryColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            child: Container(
                              alignment: Alignment.center,
                              child: Card(
                                elevation: 4,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: IconButton(
                                    padding: EdgeInsets.all(0),
                                    icon: Icon(
                                      MdiIcons.qrcodeScan,
                                      color:
                                          Theme.of(context).appBarTheme.color,
                                    ),
                                    iconSize: 160,
                                    color: Theme.of(context).primaryColor,
                                    onPressed: () {
                                      scanQR().then((value) {
                                        if (value != "-1") {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) {
                                                if (_radioValue == 0) {
                                                  return CallibrationScreen(
                                                    id: value.trim(),
                                                  );
                                                } else if (_radioValue == 1) {
                                                  return DataScreen(
                                                    id: value.trim(),
                                                  );
                                                } else if (_radioValue == 2) {
                                                  // return CheckScreen(
                                                  //   id: value.trim(),
                                                  // );
                                                  return StatusSensorScreen(
                                                    id: value.trim(),
                                                  );
                                                } else if (_radioValue == 3) {
                                                  return SettingScreen(
                                                    id: value.trim(),
                                                  );
                                                }
                                              },
                                            ),
                                          );
                                        }
                                      });
                                    }),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "OR",
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 12),
                            ),
                          ),
                          Row(
                            children: [],
                          ),
                          Form(
                            key: _formKey,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    flex: 5,
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                        labelText: 'Device ID',
                                        hintText: 'Device ID',
                                        prefixIcon: Icon(MdiIcons.chip),
                                        contentPadding: EdgeInsets.fromLTRB(
                                            20.0, 15.0, 20.0, 15.0),
                                        border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(
                                            32.0,
                                          ),
                                        ),
                                      ),
                                      validator: (value) {
                                        return null;
                                      },
                                      onSaved: (value) {
                                        _deviceId = value;
                                      },
                                    ),
                                  ),

                                  // IconButton(
                                  //   icon: Icon(
                                  //     Icons.check_circle,
                                  //     size: 30,
                                  //     color: Theme.of(context).accentColor,
                                  //   ),
                                  //   onPressed: () {
                                  //     _formKey.currentState.save();
                                  //     if (_deviceId.trim().length > 0) {
                                  //       Navigator.push(
                                  //         context,
                                  //         MaterialPageRoute(
                                  //           builder: (_) {
                                  //             if (_radioValue == 0) {
                                  //               return DataScreen(
                                  //                 id: _deviceId.trim(),
                                  //               );
                                  //             } else if (_radioValue == 1) {
                                  //               return CallibrationScreen(
                                  //                 id: _deviceId.trim(),
                                  //               );
                                  //             } else if (_radioValue == 2) {
                                  //               return CheckScreen(
                                  //                 id: _deviceId.trim(),
                                  //               );
                                  //             } else if (_radioValue == 3) {
                                  //               return SettingScreen(
                                  //                 id: _deviceId.trim(),
                                  //               );
                                  //             }
                                  //           },
                                  //         ),
                                  //       );
                                  //     }
                                  //   },
                                  // ),
                                  // ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 12),
                            child: ButtonTheme(
                              height: 50,
                              minWidth: 150,
                              child: RaisedButton(
                                visualDensity: VisualDensity.comfortable,
                                child: Text(
                                  "OK",
                                  style: TextStyle(color: Colors.white),
                                ),
                                color: Theme.of(context).accentColor,
                                onPressed: () {
                                  _formKey.currentState.save();
                                  if (_deviceId.trim().length > 0) {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (_) {
                                          if (_radioValue == 0) {
                                            return CallibrationScreen(
                                              id: _deviceId.trim(),
                                            );
                                          } else if (_radioValue == 1) {
                                            return DataScreen(
                                              id: _deviceId.trim(),
                                            );
                                          } else if (_radioValue == 2) {
                                            // return CheckScreen(
                                            //   id: _deviceId.trim(),
                                            // );
                                            return StatusSensorScreen(
                                              id: _deviceId.trim(),
                                            );
                                          } else if (_radioValue == 3) {
                                            return SettingScreen(
                                              id: _deviceId.trim(),
                                            );
                                          }
                                        },
                                      ),
                                    );
                                  }
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                              ),
                            ),
                          ),

                          // Padding(
                          //   padding: const EdgeInsets.symmetric(vertical: 10.0),
                          //   child: RaisedButton(
                          //     child: Text(
                          //       "Process",
                          //       style: TextStyle(fontSize: 16),
                          //     ),
                          //     onPressed: () {
                          //       _formKey.currentState.save();
                          //       if (_deviceId.trim().length > 0) {
                          //         Navigator.push(
                          //           context,
                          //           MaterialPageRoute(
                          //             builder: (context) => _radioValue == 0
                          //                 ? CallibrationScreen(
                          //                     id: _deviceId.trim(),
                          //                   )
                          //                 : CheckScreen(
                          //                     id: _deviceId.trim(),
                          //                   ),
                          //           ),
                          //         );
                          //       }
                          //     },
                          //     shape: RoundedRectangleBorder(
                          //       borderRadius: BorderRadius.circular(30),
                          //     ),
                          //     padding: EdgeInsets.symmetric(
                          //         horizontal: 50.0, vertical: 10.0),
                          //     color: Theme.of(context).accentColor,
                          //     textColor:
                          //         Theme.of(context).textTheme.bodyText2.color,
                          //   ),
                          // )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
