import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/input_control.dart';
import '../widgets/control_sender.dart';
import '../providers/controls.dart';
import '../widgets/control_item.dart';
import '../models/control.dart';
import '../models/mqttClientWrapper.dart';

class ControlScreen extends StatefulWidget {
  @override
  _ControlScreenState createState() => _ControlScreenState();
}

class _ControlScreenState extends State<ControlScreen> {
  void startModal(BuildContext ctx, String id) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(25.0)),
      ),
      context: ctx,
      builder: (_) {
        return AddControl(id);
      },
    );
  }

  void startModalControl(BuildContext ctx, String id, Function sendHandler) {
    showModalBottomSheet(
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(25.0)),
      ),
      context: ctx,
      builder: (_) {
        return changeValueControl(id, sendHandler);
      },
    );
  }

  MQTTClientWrapper mqttClientWrapper;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void send_data(String id) {
    if (mqttClientWrapper == null) {
    } else {
      mqttClientWrapper.disconnect();
    }

    mqttClientWrapper = MQTTClientWrapper(
        serverUri:
            Provider.of<Controls>(context, listen: false).findById(id).ip,
        port: int.parse(
            Provider.of<Controls>(context, listen: false).findById(id).port));
    mqttClientWrapper.prepareMqttClient().then((value) =>
        mqttClientWrapper.publish(
            Provider.of<Controls>(context, listen: false).findById(id).topic,
            Provider.of<Controls>(context, listen: false)
                .findById(id)
                .value
                .toString()));
  }

  void deleteControl(String id) {
    Provider.of<Controls>(context, listen: false).deleteProduct(id);
  }

  Controls controlsData;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    controlsData = Provider.of<Controls>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppBar(
          brightness: Brightness.dark,
          title: const Text(
            "Control",
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                startModal(context, null);
              },
              child: Icon(
                Icons.add,
                color: Theme.of(context).textTheme.headline1.color,
              ),
            )
          ],
        ),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.all(14),
            itemCount: controlsData.items.length,
            itemBuilder: (context, index) => ControlItem(
              controlsData.items[index],
              index,
              startModal,
              startModalControl,
              send_data,
              deleteControl,
            ),
          ),
        )
      ],
    );
  }
}
