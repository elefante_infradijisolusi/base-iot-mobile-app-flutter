import 'dart:math';

import 'package:flutter/material.dart';
import '../models/espUser.dart';
import 'package:intl/intl.dart';

class SensorScreen extends StatefulWidget {
  String id;
  String name;
  int type;
  Map initialData;
  String unit;
  SensorScreen({this.id, this.name, this.type, this.initialData, this.unit});
  @override
  _SensorScreenState createState() => _SensorScreenState();
}

class _SensorScreenState extends State<SensorScreen> {
  EspUser espController = EspUser();
  DateFormat dateFormat = DateFormat("HH:mm:ss");

  double convertData(int type, dynamic data) {
    if (type == 0) {
      if (data["value"] == null) {
        return (-1);
      } else {
        if (double.tryParse(data["value"]) == null) {
          return (-1);
        } else {
          final double val =
              double.parse(data["value"]) * double.parse(data["a"]) / 10000;
          return (val);
        }
      }
    } else if (type == 1) {
      if (data["value"] == null) {
        return (-1);
      } else {
        if (double.tryParse(data["value"]) == null) {
          return (-1);
        } else {
          double val;
          if (widget.name.toLowerCase().contains("temperature")) {
            val = double.parse(data["value"]);
          } else {
            val = (double.parse(data["value"]) - double.parse(data["b"])) *
                (double.parse(data["a"]) / 10000);
          }
          return (val);
        }
      }
    } else if (type == 2) {
      if (data["value"] == null) {
        return (-1);
      } else {
        if (double.tryParse(data["value"]) == null) {
          return (-1);
        } else {
          final double val = double.parse(data["value"]);
          return (val);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text(
          widget.name,
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: Center(
        child: StreamBuilder(
          initialData: widget.initialData,
          stream: Stream.periodic(Duration(seconds: 3)).asyncMap(
            (event) => espController.getSingleSensor(widget.type, widget.id),
          ),
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data != null) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "${convertData(widget.type, snapshot.data).toStringAsFixed(2)} ${widget.unit}",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 48,
                      color: Colors.green,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Time : ${dateFormat.format(DateTime.now())}",
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 14,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
    );
  }
}
