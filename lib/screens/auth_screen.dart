import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';

enum AuthMode { Signup, Login }

class AuthScreen extends StatelessWidget {
  static const routeName = '/auth';

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    // final transformConfig = Matrix4.rotationZ(-8 * pi / 180);
    // transformConfig.translate(-10.0);
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 155.0,
                  child: Image.asset(
                    'assets/logo-black.png',
                    fit: BoxFit.contain,
                  ),
                ),
                Container(
                  child: AuthCard(),
                )
              ],
            ),
          ),
        ),
      ),
    );

    // Scaffold(
    //   // resizeToAvoidBottomInset: false,
    //   body: Stack(
    //     children: <Widget>[
    //       Container(
    //         decoration: BoxDecoration(
    //           color: Color(0xFF424C58),
    //         ),
    //       ),
    //       SingleChildScrollView(
    //         child: Container(
    //           height: deviceSize.height,
    //           width: deviceSize.width,
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             crossAxisAlignment: CrossAxisAlignment.center,
    //             children: <Widget>[
    //               Flexible(
    //                 child: Container(
    //                   margin: EdgeInsets.only(bottom: 20.0),
    //                   padding:
    //                       EdgeInsets.symmetric(vertical: 8.0, horizontal: 94.0),
    //                   transform: Matrix4.rotationZ(-8 * pi / 180)
    //                     ..translate(-10.0),
    //                   decoration: BoxDecoration(
    //                     borderRadius: BorderRadius.circular(20),
    //                     color: Colors.deepOrange.shade900,
    //                     boxShadow: [
    //                       BoxShadow(
    //                         blurRadius: 8,
    //                         color: Colors.black26,
    //                         offset: Offset(0, 2),
    //                       )
    //                     ],
    //                   ),
    //                   child: Text(
    //                     'MyShop',
    //                     style: TextStyle(
    //                       color: Theme.of(context).accentColor,
    //                       fontSize: 50,
    //                       fontWeight: FontWeight.normal,
    //                     ),
    //                   ),
    //                 ),
    //               ),
    //               Flexible(
    //                 flex: deviceSize.width > 600 ? 2 : 1,
    //                 child: AuthCard(),
    //               ),
    //             ],
    //           ),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}

class AuthCard extends StatefulWidget {
  const AuthCard({
    Key key,
  }) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.Login;
  Map<String, String> _authData = {'email': '', 'password': '', 'username': ''};
  var _isLoading = false;
  final _passwordController = TextEditingController();

  void _showErrorDialog(String message) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('An Error Occured!'),
        content: Text(message),
        actions: [
          FlatButton(
            child: Text("Okey"),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          )
        ],
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });

    try {
      if (_authMode == AuthMode.Login) {
        await Provider.of<Auth>(context, listen: false).login(
          _authData['email'],
          _authData['password'],
        );
        // Log user in
      } else {
        // Sign user up
        await Provider.of<Auth>(context, listen: false).signup(
          _authData['email'],
          _authData['password'],
          _authData['username'],
        );
      }
    } catch (error) {}

    setState(() {
      _isLoading = false;
    });
  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.Login) {
      setState(() {
        _authMode = AuthMode.Signup;
      });
    } else {
      setState(() {
        _authMode = AuthMode.Login;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          if (_authMode == AuthMode.Signup)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: TextFormField(
                key: Key("1"),
                enabled: _authMode == AuthMode.Signup,
                decoration: InputDecoration(
                  labelText: 'User Name',
                  hintText: 'User Name',
                  prefixIcon: Icon(Icons.person_outline),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      32.0,
                    ),
                  ),
                ),
                validator: _authMode == AuthMode.Signup
                    ? (value) {
                        if (value.isEmpty) {
                          return 'Fill correct user name';
                        }
                      }
                    : null,
                onSaved: (value) {
                  _authData['username'] = value;
                },
              ),
            ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: TextFormField(
              key: Key("2"),
              decoration: InputDecoration(
                labelText: 'E-Mail',
                hintText: 'E-Mail',
                prefixIcon: Icon(Icons.alternate_email),
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    32.0,
                  ),
                ),
              ),
              keyboardType: TextInputType.emailAddress,
              validator: (value) {
                if (value.isEmpty || !value.contains('@')) {
                  return 'Invalid email!';
                }
                return null;
              },
              onSaved: (value) {
                _authData['email'] = value;
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: TextFormField(
              key: Key("3"),
              decoration: InputDecoration(
                labelText: 'Password',
                hintText: 'Password',
                prefixIcon: Icon(Icons.lock_outline),
                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    32.0,
                  ),
                ),
              ),
              obscureText: true,
              controller: _passwordController,
              validator: (value) {
                if (value.isEmpty || value.length < 5) {
                  return 'Password is too short!';
                }
              },
              onSaved: (value) {
                _authData['password'] = value;
              },
            ),
          ),
          if (_authMode == AuthMode.Signup)
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: TextFormField(
                key: Key("4"),
                enabled: _authMode == AuthMode.Signup,
                decoration: InputDecoration(
                  labelText: 'Confirm Password',
                  hintText: 'Confirm Password',
                  prefixIcon: Icon(Icons.lock_outline),
                  contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(
                      32.0,
                    ),
                  ),
                ),
                obscureText: true,
                validator: _authMode == AuthMode.Signup
                    ? (value) {
                        if (value != _passwordController.text) {
                          return 'Passwords do not match!';
                        }
                      }
                    : null,
              ),
            ),
          SizedBox(
            height: 30,
          ),
          if (_isLoading)
            CircularProgressIndicator()
          else
            RaisedButton(
              child: Text(_authMode == AuthMode.Login ? 'LOGIN' : 'SIGN UP'),
              onPressed: _submit,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              padding: EdgeInsets.symmetric(horizontal: 70.0, vertical: 12.0),
              color: Color(0xFF424C58),
              textColor: Theme.of(context).textTheme.bodyText2.color,
            ),
          FlatButton(
            child: Text('${_authMode == AuthMode.Login ? 'SIGNUP' : 'LOGIN'}'),
            onPressed: _switchAuthMode,
            padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 4),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            textColor: Theme.of(context).accentColor,
          ),
        ],
      ),
    );
  }
}
