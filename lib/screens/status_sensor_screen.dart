import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import '../models/espUser.dart';
import 'package:intl/intl.dart';

class StatusSensorScreen extends StatefulWidget {
  String id;
  StatusSensorScreen({
    this.id,
  });
  @override
  _StatusSensorScreenState createState() => _StatusSensorScreenState();
}

class _StatusSensorScreenState extends State<StatusSensorScreen> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  Future<bool> _refresh() async {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> _dataStatusSensor = [
      {
        "name": "r",
        "type": "fuse-break",
        "category": "jalur-1",
        "value": false,
      },
      {
        "name": "s",
        "type": "fuse-break",
        "category": "jalur-1",
        "value": false,
      },
      {
        "name": "t",
        "type": "fuse-break",
        "category": "jalur-1",
        "value": false,
      },
      {
        "name": "r",
        "type": "fuse-break",
        "category": "jalur-2",
        "value": false,
      },
      {
        "name": "s",
        "type": "fuse-break",
        "category": "jalur-2",
        "value": false,
      },
      {
        "name": "t",
        "type": "fuse-break",
        "category": "jalur-2",
        "value": false,
      },
      {
        "name": "r",
        "type": "fuse-break",
        "category": "jalur-3",
        "value": true,
      },
      {
        "name": "s",
        "type": "fuse-break",
        "category": "jalur-3",
        "value": false,
      },
      {
        "name": "t",
        "type": "fuse-break",
        "category": "jalur-3",
        "value": false,
      },
    ];
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: Text(
          "Status Sensor",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        actions: <Widget>[
          new IconButton(
              icon: const Icon(Icons.refresh),
              tooltip: 'Refresh',
              onPressed: () {
                _refreshIndicatorKey.currentState.show();
              }),
        ],
      ),
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView.count(
              crossAxisCount: 3,
              children: _dataStatusSensor.map<Widget>((e) {
                return Padding(
                  padding: EdgeInsets.all(12.0),
                  child: Card(
                    elevation: 5,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                        color: e["value"] ? Colors.green : Colors.red,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            e["category"].toString().toLowerCase(),
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            e["name"].toString().toUpperCase(),
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            e["value"] ? "ON" : "OFF",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: e["value"] ? Colors.green : Colors.red,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // child: Card(
                  //   color: e["value"] ? Colors.green : Colors.red,
                  //   elevation: 2,
                  //   child: Padding(
                  //     padding: const EdgeInsets.all(8.0),
                  //     child: Center(
                  //       child: Column(
                  //         mainAxisAlignment: MainAxisAlignment.start,
                  //         children: [
                  //           Text(e["category"]),
                  //           Spacer(),
                  //           Text(e["name"]),
                  //           Spacer(),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),
                );
              }).toList()
              // _dataStatusSensor.map<Widget>((key, value) => return Center()).toList(),
              // <Widget>[
              //   Container(
              //     color: Colors.yellowAccent,
              //     child: Center(
              //       child: Text(
              //         "1",
              //         style: TextStyle(fontSize: 24.0),
              //       ),
              //     ),
              //   ),
              //   Container(
              //     color: Colors.blueAccent,
              //     child: Center(
              //       child: Text(
              //         "2",
              //         style: TextStyle(fontSize: 24.0),
              //       ),
              //     ),
              //   ),
              //   Container(
              //     color: Colors.brown,
              //     child: Center(
              //       child: Text(
              //         "3",
              //         style: TextStyle(fontSize: 24.0),
              //       ),
              //     ),
              //   ),
              //   Container(
              //     color: Colors.orange,
              //     child: Center(
              //       child: Text(
              //         "4",
              //         style: TextStyle(fontSize: 24.0),
              //       ),
              //     ),
              //   ),
              // ],
              ),
        ),
      ),
    );
  }
}
