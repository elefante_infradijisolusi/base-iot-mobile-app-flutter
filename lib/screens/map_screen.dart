import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:io' show Platform;

import 'package:location/location.dart';
import 'package:flushbar/flushbar.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';

import 'package:provider/provider.dart';

import '../providers/auth.dart';

class MapScreen extends StatefulWidget {
  final Function showFlushbarHandler;
  MapScreen(this.showFlushbarHandler);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  MapboxMapController controller;
  Symbol _markerSymbol;
  int _symbolCount;
  Timer timer;

  bool isSwitched = false;
  LatLng center = LatLng(-7.265029807528294, 112.78100967407225);
  double zoom = 13.0;
  bool _isanimate = false;

  Map<String, String> symbol_marker_id = {};
  Map<String, dynamic> symbol_marker_data = {};
  Map<String, Symbol> symbol_marker = {};

  String convert(LocationData input) {
    return "{\"latitude\":${input.latitude},\"longitude\":${input.longitude}, \"time\":${DateTime.now().toIso8601String()}}";
  }

  /// Adds an asset image to the currently displayed style
  Future<void> addImageFromAsset(String name, String assetName) async {
    final ByteData bytes = await rootBundle.load(assetName);
    final Uint8List list = bytes.buffer.asUint8List();
    return controller.addImage(name, list);
  }

  SymbolOptions _getSymbolOptions(String iconImage) {
    return SymbolOptions(
      geometry: LatLng(center.latitude, center.longitude),
      iconImage: iconImage,
    );
  }

  Color findColor(String message) {
    if (message.toLowerCase().contains("heat") ||
        message.toLowerCase().contains("fuse")) {
      return Colors.red;
    } else if (message.toLowerCase().contains("balance") ||
        message.toLowerCase().contains("value")) {
      return Colors.orange;
    } else {
      return Colors.green;
    }
  }

  void _onSymbolTapped(Symbol symbol) {
    widget.showFlushbarHandler(
      symbol.options.textField,
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: symbol_marker_data[symbol.id]["message"]
            .reversed
            .toList()
            .map<Widget>((item) => Center(
                  child: Container(
                    padding: EdgeInsets.all(4.0),
                    margin: EdgeInsets.only(right: 5),
                    decoration: BoxDecoration(
                      // color: findColor(item),
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: findColor(item),
                        width: 1.0,
                      ),
                    ),
                    child: Center(
                        child: Text(
                      item,
                      style: TextStyle(
                        fontSize: 10,
                        color: findColor(item),
                        fontWeight: FontWeight.w400,
                      ),
                    )),
                  ),
                ))
            .toList(),
      ),
    );
  }

  void _onMapCreated(MapboxMapController mapController) {
    controller = mapController;
    controller.onSymbolTapped.add(_onSymbolTapped);

    Provider.of<Auth>(context, listen: false).getDataSensor().then(
          (datajson) => {
            for (var i = 0; i < datajson.length; i++)
              {
                controller
                    .addSymbol(
                  SymbolOptions(
                    geometry: LatLng(datajson[i]['position']['latitude'],
                        datajson[i]['position']['longitude']),
                    iconImage: selecMarker(datajson[i]["deviceStatus"]["code"]),
                    iconAnchor: 'bottom',
                    textField: datajson[i]['name'],
                    textSize: 15,
                    iconSize: 1.5,
                    textOffset: Offset(0, -3.8),
                  ),
                )
                    .then(
                  (value_symbol) {
                    symbol_marker_id.putIfAbsent(
                        datajson[i]['moduleId'], () => value_symbol.id);
                    symbol_marker.putIfAbsent(
                        value_symbol.id, () => value_symbol);
                    symbol_marker_data.putIfAbsent(
                      value_symbol.id,
                      () => datajson[i]["deviceStatus"],
                    );
                  },
                ),
              },
          },
        );
  }

  String selecMarker(code) {
    if (code > 2) {
      return "danger_symbol";
    } else if (code <= 2 && code > 0) {
      return "warning_symbol";
    } else {
      return "marker_symbol";
    }
  }

  void _onStyleLoaded() {
    addImageFromAsset("marker_symbol", "assets/symbol/marker.png");
    addImageFromAsset("warning_symbol", "assets/symbol/warning.png");
    addImageFromAsset("danger_symbol", "assets/symbol/danger.png");
  }

  void _addsymbol(String iconImage) {
    if (_markerSymbol != null) {
      controller.removeSymbol(_markerSymbol).then((value) {
        _markerSymbol = null;
      });
    } else {
      controller.addSymbol(
          _getSymbolOptions(iconImage), {'name': "marker"}).then((value) {
        _markerSymbol = value;
      });
    }
    ;
  }

  @override
  void dispose() {
    controller?.onSymbolTapped?.remove(_onSymbolTapped);
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppBar(
          brightness: Brightness.dark,
          title: const Text(
            "Map",
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () {
                controller.animateCamera(
                  CameraUpdate.newCameraPosition(
                    CameraPosition(
                      bearing: 0.0,
                      tilt: 0.0,
                      zoom: zoom,
                      target: center,
                    ),
                  ),
                );
              },
              child: Icon(
                Icons.my_location,
                color: Colors.white,
              ),
            )
          ],
        ),
        Expanded(
          child: MapboxMap(
            onMapCreated: _onMapCreated,
            onStyleLoadedCallback: _onStyleLoaded,
            initialCameraPosition: CameraPosition(
              bearing: 0.0,
              tilt: 0.0,
              zoom: zoom,
              target: center,
            ),
          ),
        )
      ],
    );
  }
}
