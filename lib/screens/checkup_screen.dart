import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

import 'package:provider/provider.dart';
import '../providers/auth.dart';

class CheckScreen extends StatefulWidget {
  final String id;
  const CheckScreen({this.id});

  @override
  _CheckScreenState createState() => _CheckScreenState();
}

class _CheckScreenState extends State<CheckScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  String _message = "";

  bool _isloading = false;

  List<List<Map<String, dynamic>>> data = [
    [
      {"name": "Temp. Sensor", "value": false},
      {"name": "Current Sensor", "value": false}
    ],
    [
      {"name": "Voltage Sensor", "value": false},
      {"name": "Battery Sensor", "value": false}
    ]
  ];

  List<Asset> images = List<Asset>();
  String _error = 'No Error Dectected';

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return Padding(
          padding: const EdgeInsets.all(3.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: Theme.of(context).accentColor),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              child: AssetThumb(
                asset: asset,
                width: 300,
                height: 300,
              ),
            ),
          ),
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 6,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#424C58",
          actionBarTitle: "Choose Photos",
          allViewTitle: "All Photos",
          useDetailsView: true,
          selectCircleStrokeColor: "#1CC7DE",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.id);
    return Scaffold(
      appBar: AppBar(
        title: Text("Check-up"),
        actions: [
          IconButton(
            icon: Icon(Icons.cloud_upload),
            onPressed: () {
              _formKey.currentState.save();
              showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                  title: Text('Are you sure ?'),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.of(ctx).pop(false);
                      },
                      child: Text("No"),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(ctx).pop(true);
                      },
                      child: Text("Yes"),
                    )
                  ],
                ),
              ).then((value) {
                if (value) {
                  List<Map<String, dynamic>> dataChecklist = data
                      .fold<List<Map<String, dynamic>>>(
                          [],
                          (previousValue, element) =>
                              [...previousValue, ...element]);

                  setState(() {
                    _isloading = true;
                  });

                  Provider.of<Auth>(context, listen: false)
                      .postReportCheckWithImage(
                    images,
                    widget.id,
                    "check-up",
                    jsonEncode({"check": dataChecklist, "message": _message})
                        .replaceAll("\\", ""),
                    "Percentace check true (more is better)",
                    100 *
                        dataChecklist
                            .where((element) => element["value"])
                            .length /
                        dataChecklist.length,
                  )
                      .then((value) {
                    if (value == 200) {
                      setState(() {
                        images = [];
                        _isloading = false;
                      });
                    }
                  });
                }
              });
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: loadAssets,
        backgroundColor: Theme.of(context).accentColor,
        child: Icon(
          Icons.camera_alt,
          color: Colors.white,
        ),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Card(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Check List",
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                      Table(
                        children: [
                          ...data
                              .map<TableRow>(
                                (rowItem) => TableRow(
                                  children: [
                                    ...rowItem
                                        .map<TableCell>(
                                          (e) => TableCell(
                                            child: Container(
                                              child: Row(
                                                children: [
                                                  Checkbox(
                                                    value: e["value"],
                                                    onChanged: (bool value) {
                                                      setState(() {
                                                        e["value"] = value;
                                                      });
                                                    },
                                                  ),
                                                  Text(
                                                    e["name"],
                                                    style: TextStyle(
                                                        color: Theme.of(context)
                                                            .primaryColor),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                        .toList()
                                  ],
                                ),
                              )
                              .toList()
                        ],
                      ),
                      Text(
                        "Message :",
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Message',
                            hintText: 'Message',
                            contentPadding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          ),
                          keyboardType: TextInputType.multiline,
                          minLines: 1,
                          maxLines: 10,
                          validator: (value) {
                            return null;
                          },
                          onSaved: (value) {
                            if (value.isNotEmpty) {
                              _message = value;
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                child: _isloading
                    ? Expanded(
                        child: Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.red,
                          ),
                        ),
                      )
                    : Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: buildGridView(),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
