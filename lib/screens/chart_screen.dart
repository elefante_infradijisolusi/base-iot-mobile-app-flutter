import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../screens/detail_node_screen.dart';
import '../widgets/node_item.dart';
import '../providers/auth.dart';

class ChartScreen extends StatefulWidget {
  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  Map<String, dynamic> dataJson;

  IO.Socket socket = IO.io('http://192.168.1.9:3000', <String, dynamic>{
    'transports': ['websocket'],
    'autoConnect': false,
  });

  void _initSocket() {
    socket.connect();
    socket.on('connect', (_) {
      print("Connected");
    });

    socket.on('index data', (data) {
      print(data);
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // socket.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    // _initSocket();
    super.initState();

    Provider.of<Auth>(context, listen: false)
        .getDataSensor()
        .then((value) => print(value));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: AppBar(
            brightness: Brightness.dark,
            title: const Text(
              "Devices",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
        Expanded(
          child: StreamBuilder(
            initialData: null,
            stream: Stream.periodic(Duration(seconds: 3)).asyncMap(
              (i) => Provider.of<Auth>(context, listen: false).getDataSensor(),
            ),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) => NodeItem(
                    index: index + 1,
                    name: snapshot.data[index]["name"],
                    category: snapshot.data[index]["category"],
                    status: snapshot.data[index]["deviceStatus"],
                    clickHandler: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailNode(
                            args: snapshot.data[index]["moduleId"],
                          ),
                        ),
                      );
                    },
                  ),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ),
      ],
    );
  }
}
