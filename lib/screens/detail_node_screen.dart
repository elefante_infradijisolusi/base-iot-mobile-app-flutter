import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../widgets/sensor_item.dart';
import '../widgets/deskripsi_node.dart';
import '../widgets/chart.dart';
import '../providers/auth.dart';

class DetailNode extends StatefulWidget {
  String args;
  DetailNode({this.args});

  @override
  _DetailNodeState createState() => _DetailNodeState();
}

class _DetailNodeState extends State<DetailNode> {
  int _current = 0;
  CarouselController buttonCarouselController = CarouselController();
  Map<String, dynamic> dataJson;
  List<Widget> chartSlider;

  @override
  void initState() {
    // chartSlider = createLineWidget();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        title: const Text(
          "Details",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      body: StreamBuilder(
        initialData: null,
        stream: Stream.periodic(Duration(seconds: 2)).asyncMap(
          (event) => Provider.of<Auth>(context, listen: false)
              .getDataSensorSingle(widget.args),
        ),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Map> dataSensor = [];
            List<Map> dataSensorAvg = [];

            for (var i = 0; i < snapshot.data[0]["sensors"].length; i++) {
              dataSensorAvg.add({
                "parameter": snapshot.data[0]["sensors"][i]["parameter"],
                "unit": snapshot.data[0]["sensors"][i]["unit"],
                "value": []
              });
              for (var j = 0;
                  j < snapshot.data[0]["sensors"][i]["sensor"].length;
                  j++) {
                Map dataCurrent = snapshot.data[0]["sensors"][i]["sensor"][j];
                dataCurrent["unit"] = snapshot.data[0]["sensors"][i]["unit"];
                dataSensor.add(dataCurrent);

                dataSensorAvg[i]["value"].add(dataCurrent["currentValue"]);
              }
            }
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    children: [
                      CarouselSlider(
                        items: [
                          DetailView(
                            name: snapshot.data[0]['name'],
                            kategori: snapshot.data[0]['category'],
                            status: snapshot.data[0]['deviceStatus'],
                            numSensor: dataSensor.length,
                            dataSensorAvg: dataSensorAvg,
                            deviceStatus: snapshot.data[0]["deviceStatus"],
                          )
                        ],
                        carouselController: buttonCarouselController,
                        options: CarouselOptions(
                          enableInfiniteScroll: false,
                          viewportFraction: 1,
                          onPageChanged: (index, reason) {
                            setState(
                              () {
                                _current = index;
                              },
                            );
                          },
                        ),
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: List<int>.generate((2), (i) => i)
                      //       .map<Widget>((index) {
                      //     return Container(
                      //       width: 8.0,
                      //       height: 8.0,
                      //       margin: EdgeInsets.symmetric(
                      //           vertical: 10.0, horizontal: 2.0),
                      //       decoration: BoxDecoration(
                      //         shape: BoxShape.circle,
                      //         color: _current == index
                      //             ? Color.fromRGBO(0, 0, 0, 0.9)
                      //             : Color.fromRGBO(0, 0, 0, 0.4),
                      //       ),
                      //     );
                      //   }).toList(),
                      // )
                    ],
                  ),
                ),
                Center(
                  child: Text(
                    "Sensor List",
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: ListView.builder(
                      itemCount: dataSensor.length,
                      itemBuilder: (context, index) {
                        return SensorItem(
                          name: dataSensor[index]['subParam'],
                          index: (index + 1),
                          value: dataSensor[index]["currentValue"].toString(),
                          unit: dataSensor[index]['unit'],
                          clickHandler: () {},
                          code: dataSensor[index]['sensorStatus']["code"],
                        );
                      },
                    ),
                  ),
                )
              ],
            );
            // return Column(
            //   crossAxisAlignment: CrossAxisAlignment.stretch,
            //   children: [
            //     Padding(
            //       padding: const EdgeInsets.only(top: 8.0),
            //       child: Column(
            //         children: [
            //           CarouselSlider(
            //             items: [
            //               DetailView(
            //                 name: dataJson['node']['name'],
            //                 kategori: dataJson['node']['category'],
            //                 status: dataJson['node']['status'],
            //                 avg_value: dataJson['node']['avg'],
            //                 min_value: dataJson['node']['min'],
            //                 max_value: dataJson['node']['max'],
            //                 num_sensor: dataJson['sensor'].length,
            //               ),
            //               ...dataJson['sensor'].map<Widget>((item) => Container(
            //                     child: Card(
            //                         elevation: 2,
            //                         margin: EdgeInsets.all(10),
            //                         child: LineChartView(
            //                           data_chart: item['history'],
            //                           id: item['sensorid'],
            //                           name: item['name'],
            //                         )),
            //                   )),
            //             ],
            //             carouselController: buttonCarouselController,
            //             options: CarouselOptions(
            //               enableInfiniteScroll: false,
            //               viewportFraction: 1,
            //               onPageChanged: (index, reason) {
            //                 setState(() {
            //                   _current = index;
            //                 });
            //               },
            //             ),
            //           ),
            //           Row(
            //             mainAxisAlignment: MainAxisAlignment.center,
            //             children: List<int>.generate(
            //                     (dataJson['sensor'].length + 1), (i) => i)
            //                 .map<Widget>((index) {
            //               return Container(
            //                 width: 8.0,
            //                 height: 8.0,
            //                 margin: EdgeInsets.symmetric(
            //                     vertical: 10.0, horizontal: 2.0),
            //                 decoration: BoxDecoration(
            //                   shape: BoxShape.circle,
            //                   color: _current == index
            //                       ? Color.fromRGBO(0, 0, 0, 0.9)
            //                       : Color.fromRGBO(0, 0, 0, 0.4),
            //                 ),
            //               );
            //             }).toList(),
            //           )
            //         ],
            //       ),
            //     ),
            //     Center(
            //       child: Text(
            //         "Sensor List",
            //         style: TextStyle(
            //             color: Theme.of(context).accentColor, fontSize: 20),
            //       ),
            //     ),
            //     Expanded(
            //       child: Padding(
            //         padding: const EdgeInsets.all(10),
            //         child: ListView.builder(
            //           itemCount: dataJson['sensor'].length,
            //           itemBuilder: (context, index) {
            //             return SensorItem(
            //               name: dataJson['sensor'][index]['name'],
            //               index: (index + 1),
            //               value: dataJson['sensor'][index]['history']
            //                   .last['value']
            //                   .toString(),
            //               color: dataJson['sensor'][index]['color'],
            //               unit: dataJson['sensor'][index]['unit'],
            //               clickHandler: () {
            //                 setState(() {
            //                   _current = index + 1;
            //                   buttonCarouselController.jumpToPage(_current);
            //                 });
            //               },
            //             );
            //           },
            //         ),
            //       ),
            //     )
            //   ],
            // );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
