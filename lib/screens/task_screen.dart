import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

import '../providers/auth.dart';
import '../models/task.dart';

class MapUtils {
  MapUtils._();

  static Future<void> openMap(double latitude, double longitude) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }
}

class TaskScreen extends StatefulWidget {
  const TaskScreen({this.task});
  final Task task;
  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  DateFormat dateFormat = DateFormat("dd/MM/yyyy");

  String _message = "";
  bool _isloading = false;
  List<dynamic> data = [];
  List<Asset> images = List<Asset>();
  LocationData currentLocation;

  void _getLocation() async {
    var location = new Location();
    try {
      await location.getLocation().then((onValue) {
        setState(() {
          currentLocation = onValue;
        });
      });
    } catch (e) {
      if (e.code == 'PERMISSION_DENIED') {}
    }
  }

  String _error = 'No Error Dectected';
  Widget buildGridView() {
    return GridView.count(
      shrinkWrap: true,
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return Padding(
          padding: const EdgeInsets.all(3.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(color: Theme.of(context).accentColor),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              child: AssetThumb(
                asset: asset,
                width: 300,
                height: 300,
              ),
            ),
          ),
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 6,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#424C58",
          actionBarTitle: "Choose Photos",
          allViewTitle: "All Photos",
          useDetailsView: true,
          selectCircleStrokeColor: "#1CC7DE",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }

  @override
  void initState() {
    super.initState();
    data = widget.task.getChecklist();
    _getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Task"),
        actions: [
          IconButton(
            icon: Icon(Icons.cloud_upload),
            onPressed: () {
              _formKey.currentState.save();
              showDialog(
                context: context,
                builder: (ctx) => AlertDialog(
                  title: Text('Are you sure ?'),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.of(ctx).pop(false);
                      },
                      child: Text("No"),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(ctx).pop(true);
                      },
                      child: Text("Yes"),
                    )
                  ],
                ),
              ).then((value) {
                if (value) {
                  List<Map<String, dynamic>> dataChecklist =
                      data.fold<List<Map<String, dynamic>>>(
                    [],
                    (previousValue, element) => [...previousValue, ...element],
                  );
                  Map<String, dynamic> dataSend = {
                    "checkList": {},
                    "actualLocation": json.encode(
                      {
                        "latitude": currentLocation.latitude,
                        "longitude": currentLocation.longitude,
                        "place": ""
                      },
                    ),
                    "targetLocation": json.encode(widget.task.targetLocation),
                    "comment": _message,
                    "id": widget.task.id
                  };

                  for (var i = 0; i < dataChecklist.length; i++) {
                    dataSend["checkList"].putIfAbsent(
                      dataChecklist[i]["name"],
                      () => dataChecklist[i]["value"],
                    );
                  }
                  dataSend["checkList"] = json.encode(dataSend["checkList"]);
                  setState(() {
                    _isloading = true;
                  });

                  Provider.of<Auth>(context, listen: false)
                      .postTask(images, dataSend)
                      .then((value) {
                    if (value == 200) {
                      setState(() {
                        images = [];
                        _isloading = false;
                      });
                    }
                  });
                }
              });
            },
          ),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            width: 50.0,
            height: 50.0,
            margin: EdgeInsets.only(bottom: 10),
            child: RawMaterialButton(
              shape: CircleBorder(),
              fillColor: Theme.of(context).accentColor,
              elevation: 0.0,
              child: Icon(
                Icons.map,
                color: Colors.white,
              ),
              onPressed: () {
                MapUtils.openMap(widget.task.targetLocation["latitude"],
                    widget.task.targetLocation["longitude"]);
              },
            ),
          ),
          FloatingActionButton(
            onPressed: loadAssets,
            backgroundColor: Theme.of(context).accentColor,
            child: Icon(
              Icons.camera_alt,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Card(
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Description",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Table(
                            columnWidths: {
                              0: FlexColumnWidth(1),
                              1: FlexColumnWidth(4),
                            },
                            border: TableBorder(
                              // horizontalInside: BorderSide(
                              //   width: 0,
                              //   color: Colors.grey,
                              //   style: BorderStyle.solid,
                              // ),
                              top: BorderSide(
                                width: 0,
                                color: Colors.grey,
                                style: BorderStyle.solid,
                              ),
                              bottom: BorderSide(
                                width: 0,
                                color: Colors.grey,
                                style: BorderStyle.solid,
                              ),
                              left: BorderSide(
                                width: 0,
                                color: Colors.grey,
                                style: BorderStyle.solid,
                              ),
                              right: BorderSide(
                                width: 0,
                                color: Colors.grey,
                                style: BorderStyle.solid,
                              ),
                            ),
                            children: [
                              TableRow(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Title',
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      widget.task.title,
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Detail',
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      widget.task.message,
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Location',
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      widget.task.targetLocation["place"],
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ],
                              ),
                              TableRow(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Time',
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      '${dateFormat.format(widget.task.startDate)} - ${dateFormat.format(widget.task.endDate)}',
                                      style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Text(
                          "Check List",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Table(
                          children: [
                            ...data
                                .map<TableRow>(
                                  (rowItem) => TableRow(
                                    children: [
                                      ...rowItem
                                          .map<TableCell>(
                                            (e) => TableCell(
                                              child: Container(
                                                child: Row(
                                                  children: [
                                                    Checkbox(
                                                      value: e["value"],
                                                      onChanged: (bool value) {
                                                        setState(() {
                                                          e["value"] = value;
                                                        });
                                                      },
                                                    ),
                                                    Text(
                                                      e["name"],
                                                      style: TextStyle(
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColor),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          )
                                          .toList()
                                    ],
                                  ),
                                )
                                .toList()
                          ],
                        ),
                        Text(
                          "Message :",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: InputDecoration(
                              labelText: 'Message',
                              hintText: 'Message',
                              contentPadding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            ),
                            keyboardType: TextInputType.multiline,
                            minLines: 1,
                            maxLines: 10,
                            validator: (value) {
                              return null;
                            },
                            onSaved: (value) {
                              if (value.isNotEmpty) {
                                _message = value;
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: _isloading
                      ? CircularProgressIndicator(
                          backgroundColor: Colors.red,
                        )
                      : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: buildGridView(),
                        ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
