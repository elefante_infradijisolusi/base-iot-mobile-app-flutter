import 'dart:math';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:number_inc_dec/number_inc_dec.dart';

import '../providers/auth.dart';
import '../models/callibratioin.dart';
import '../models/espUser.dart';

class CallibrationScreen extends StatefulWidget {
  final String id;
  const CallibrationScreen({this.id});

  @override
  _CallibrationScreenState createState() => _CallibrationScreenState();
}

class _CallibrationScreenState extends State<CallibrationScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  // ItemCallibration itemCallibration = ItemCallibration();
  CallibrationReport reportObject = CallibrationReport(url: "192.168.4.1");
  EspUser espController = EspUser();

  Item selected;
  Map _uploadData = {};
  bool _isSending = false;
  bool _isValidate = false;
  bool _esploading = false;
  List<Item> _datadropdown = List();

  void fetchSensorList() async {
    final response = await espController.getDataAll();
    var listData = response
        .map<Item>((item) =>
            Item(id: item["id"], sensor: item["name"], type: item["type"]))
        .toList();
    listData.removeWhere(
        (Item item) => item.sensor.toLowerCase().contains("temperature"));
    setState(
      () {
        _datadropdown = listData;
      },
    );
  }

  @override
  void initState() {
    super.initState();
    fetchSensorList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        brightness: Brightness.dark,
        title: const Text(
          "Callibration",
          style: TextStyle(color: Colors.white),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.cloud_upload),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (ctx) => AlertDialog(
                    title: Text('Are you sure ?'),
                    content: Text(reportObject.getData().toString()),
                    actions: <Widget>[
                      FlatButton(
                        onPressed: () {
                          Navigator.of(ctx).pop(false);
                        },
                        child: Text("No"),
                      ),
                      FlatButton(
                        onPressed: () {
                          Navigator.of(ctx).pop(true);
                          Provider.of<Auth>(context, listen: false)
                              .postReport(
                                widget.id,
                                "calibration",
                                jsonEncode(
                                  reportObject.getData(),
                                ),
                                "deviation of past value (small is better)",
                                reportObject.getScore(),
                              )
                              .then((value) {});
                        },
                        child: Text("Yes"),
                      )
                    ],
                  ),
                );
              }),
        ],
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
            width: 50.0,
            height: 50.0,
            margin: EdgeInsets.only(bottom: 10),
            child: RawMaterialButton(
              shape: CircleBorder(),
              fillColor: (selected == null || _esploading)
                  ? Colors.grey
                  : Theme.of(context).accentColor,
              elevation: 0.0,
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () {
                if (selected != null) {
                  if (_isValidate || _esploading) {
                  } else {
                    _formKey.currentState.save();
                  }
                  if (!_esploading) {
                    setState(
                      () {
                        _esploading = true;
                      },
                    );
                    if (selected.type == 1 &&
                        espController.getVal().length < 2) {
                      espController
                          .getDataSensor(
                        1,
                        selected.id,
                      )
                          .then((value) {
                        final snackBar = SnackBar(
                          content: Text(
                            value.toString(),
                          ),
                        );
                        _scaffoldKey.currentState.showSnackBar(snackBar);

                        setState(() {
                          _isValidate = false;
                          _esploading = false;
                        });
                      });
                    } else if (selected.type == 0) {
                      espController
                          .getDataSensor(
                        0,
                        selected.id,
                      )
                          .then((value) {
                        final snackBar = SnackBar(
                          content: Text(
                            value.toString(),
                          ),
                        );
                        _scaffoldKey.currentState.showSnackBar(snackBar);
                        setState(() {
                          _isValidate = false;
                          _esploading = false;
                        });
                      });
                    } else if (selected.type == 2 &&
                        espController.getVal().length < 1) {
                      espController
                          .getDataSensor(
                        2,
                        selected.id,
                      )
                          .then((value) {
                        final snackBar = SnackBar(
                          content: Text(
                            value.toString(),
                          ),
                        );
                        _scaffoldKey.currentState.showSnackBar(snackBar);
                        setState(() {
                          _isValidate = false;
                          _esploading = false;
                        });
                      });
                    }
                  } else {}
                }
              },
            ),
          ),
          FloatingActionButton(
              backgroundColor: selected == null
                  ? Colors.grey
                  : Theme.of(context).accentColor,
              child: _isValidate
                  ? Icon(
                      Icons.file_upload,
                      color: Colors.white,
                    )
                  : Icon(
                      Icons.lock_open,
                      color: Colors.white,
                    ),
              onPressed: () {
                setState(() {
                  if (_isValidate) {
                    _isValidate = false;

                    if (selected != null) {
                      var dataCallibration =
                          espController.getSendData(selected.id);
                      var score = espController.getScore();
                      reportObject.addReport(selected.id, score,
                          selected.sensor, dataCallibration);

                      showDialog(
                        context: context,
                        builder: (ctx) => AlertDialog(
                          title: Text('Are you sure send this data to device?'),
                          content: Text(dataCallibration.toString()),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.of(ctx).pop(false);
                              },
                              child: Text("No"),
                            ),
                            FlatButton(
                              onPressed: () {
                                Navigator.of(ctx).pop(true);
                              },
                              child: Text("Yes"),
                            )
                          ],
                        ),
                      ).then(
                        (value) {
                          if (value) {
                            espController.setDataSensor(selected.id).then(
                              (res) {
                                setState(() {
                                  espController.clear();
                                });
                              },
                            );
                          }
                        },
                      );
                    }
                  } else {
                    _formKey.currentState.save();
                    _isValidate = true;
                  }
                });
              }),
        ],
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Card(
              child: Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Sersor name :",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    DropdownButton<Item>(
                      elevation: 12,
                      isExpanded: true,
                      hint: Text("Select item"),
                      value: selected,
                      onChanged: (Item value) {
                        setState(
                          () {
                            selected = value;
                            espController.clear();
                            _isSending = false;
                            _isValidate = false;
                            _esploading = false;
                          },
                        );
                      },
                      items: _datadropdown.map((item) {
                        return DropdownMenuItem(
                          child: Text(item.sensor),
                          value: item,
                        );
                      }).toList(),
                    ),
                    // SizedBox(
                    //   height: 20,
                    // ),
                    Text(
                      "Callibration Parameter :",
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontWeight: FontWeight.bold),
                    ),
                    selected != null
                        ? espController.getOldParam()
                        : SizedBox(
                            width: 0,
                          ),
                    selected != null
                        ? espController.getNewParam()
                        : SizedBox(
                            width: 0,
                          ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Divider(color: Colors.black),
            ),
            Expanded(
              child: ListView(
                children: [
                  ...espController.getVal().map<Widget>(
                    (e) {
                      return Dismissible(
                        key: ValueKey(e),
                        background: Container(
                          // margin: EdgeInsets.only(bottom: 10),
                          alignment: Alignment.centerRight,
                          // padding: EdgeInsets.symmetric(horizontal: 15),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Theme.of(context).errorColor,
                          ),
                          // color: Theme.of(context).errorColor,
                          child: Icon(
                            Icons.delete,
                            color: Theme.of(context).iconTheme.color,
                            size: 20,
                          ),
                        ),
                        direction: DismissDirection.endToStart,
                        onDismissed: (direction) {
                          setState(() {
                            espController.deleteByValue(e);
                          });
                        },
                        confirmDismiss: (direction) {
                          return showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              title: Text('Are you sure ?'),
                              content: Text('Do you want to remove this item?'),
                              actions: <Widget>[
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop(false);
                                  },
                                  child: Text("No"),
                                ),
                                FlatButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop(true);
                                  },
                                  child: Text("Yes"),
                                )
                              ],
                            ),
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(2.0),
                              child: ListTile(
                                leading: selected.type == 2
                                    ? null
                                    : CircleAvatar(
                                        child: Icon(
                                          e["actualValue"] == null
                                              ? Icons.close
                                              : Icons.check,
                                          color: Colors.white,
                                        ),
                                        backgroundColor:
                                            e["actualValue"] == null
                                                ? Colors.red
                                                : Theme.of(context).accentColor,
                                      ),
                                title: e["actualValue"] == null
                                    ? (selected.type == 2
                                        ? NumberInputWithIncrementDecrement(
                                            controller: TextEditingController(),
                                            min: 0,
                                            max: 100,
                                            initialValue: e["rawValue"].toInt(),
                                            incIconColor:
                                                Theme.of(context).accentColor,
                                            decIconColor:
                                                Theme.of(context).accentColor,
                                            incIconSize: 30,
                                            decIconSize: 30,
                                            incIconDecoration:
                                                BoxDecoration(border: null),
                                            widgetContainerDecoration:
                                                BoxDecoration(border: null),
                                            onIncrement: (newValue) {
                                              espController.updateValue(
                                                  e, newValue.toString());
                                            },
                                            onDecrement: (newValue) {
                                              espController.updateValue(
                                                  e, newValue.toString());
                                            },
                                          )
                                        : TextFormField(
                                            decoration: InputDecoration(
                                              labelText: 'Real Value',
                                              hintText: 'Real Value',
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      15.0, 10.0, 15.0, 10.0),
                                              border: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                  5.0,
                                                ),
                                              ),
                                            ),
                                            keyboardType: TextInputType.number,
                                            validator: (value) {
                                              return null;
                                            },
                                            onSaved: (value) {
                                              if (value.isNotEmpty) {
                                                espController.updateValue(
                                                    e, value);
                                              } else {
                                                espController.deleteByValue(e);
                                              }
                                            },
                                          ))
                                    : Text(
                                        "Real : " + e["actualValue"].toString(),
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor),
                                      ),
                                trailing: selected.type == 2
                                    ? null
                                    : Text(
                                        "Raw : " +
                                            e["rawValue"].toStringAsFixed(1),
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor),
                                      ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ).toList()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
