import 'dart:convert';
import 'dart:async';
import 'dart:typed_data';
import 'package:http_parser/http_parser.dart';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../helper/util.dart';

const String url = 'https://power.elefante.co.id';
// const String url = 'http://192.168.1.9:3000';

class Auth with ChangeNotifier {
  String _token;
  DateTime _expiryDate;
  String _userId;
  Timer _authTimer;

  bool get isAuth {
    return token != null;
  }

  String get token {
    if (_expiryDate != null &&
        _expiryDate.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  Future<dynamic> getTask() async {
    final response = await http.get('${url}/account-task-mobile/${_userId}',
        headers: {"Accept": "application/json", 'Authorization': _token});
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<List<dynamic>> getDataSensor() async {
    // String url = '${url}/api/getNodeData';
    final response = await http.get('${url}/api/getNodeData',
        headers: {"Accept": "application/json", 'Authorization': _token});
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<Map> updateCallibration(Map message) async {
    // String url = 'https://power.elefante.co.id/api/updateCallibration';
    final response = await http.post(
      '${url}/api/updateCallibration',
      headers: {"Accept": "application/json", 'Authorization': _token},
      body: message,
    );
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<List<dynamic>> getDataSensorSingle(String moduleId) async {
    // String url = 'https://power.elefante.co.id/api/getNodeDataSingle';
    final response = await http.post('${url}/api/getNodeDataSingle',
        headers: {"Accept": "application/json", 'Authorization': _token},
        body: {"moduleId": moduleId});
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<dynamic> postDevice(Map body) async {
    final response = await http.post(
      '${url}/api/add-device',
      headers: {"Accept": "application/json", 'Authorization': _token},
      body: body,
    );
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<dynamic> postReport(String moduleId, String type, String message,
      String info, double score) async {
    // String url = 'https://power.elefante.co.id/api/getNodeDataSingle';
    final response = await http.post('${url}/api/postReport', headers: {
      "Accept": "application/json",
      'Authorization': _token
    }, body: {
      "moduleId": moduleId,
      "time": DateTime.now().toIso8601String(),
      "type": type,
      "message": message,
      "info": info,
      "score": score.toStringAsFixed(1),
    });
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<dynamic> postTask(List listAset, Map<String, dynamic> message) async {
    Uri uri = Uri.parse('$url/api/postTask');
    http.MultipartRequest request = http.MultipartRequest("POST", uri);
    String index_image = "";
    for (var i = 0; i < listAset.length; i++) {
      ByteData byteData = await listAset[i].getByteData();
      List<int> imageData = byteData.buffer.asUint8List();

      index_image = "image";
      http.MultipartFile multipartFile = http.MultipartFile.fromBytes(
        index_image,
        imageData,
        filename: '${index_image}-${i.toString()}.jpg',
        contentType: MediaType("image", "jpg"),
      );
      request.files.add(multipartFile);
    }
    Map<String, String> headers = {
      "Accept": "application/json",
      'Authorization': _token
    };

    request.headers.addAll(headers);
    request.fields.addAll({
      "checkList": message["checkList"],
      "actualLocation": message["actualLocation"],
      "comment": message["comment"],
      "id": message["id"],
      "targetLocation": message["targetLocation"],
    });

    var response = await request.send();
    final respStr = await http.Response.fromStream(response);

    return respStr.statusCode;
  }

  Future<dynamic> postReportCheckWithImage(List listAset, String moduleId,
      String type, String message, String info, double score) async {
    Uri uri = Uri.parse('$url/api/postReport');

    http.MultipartRequest request = http.MultipartRequest("POST", uri);

    String index_image = "";

    for (var i = 0; i < listAset.length; i++) {
      ByteData byteData = await listAset[i].getByteData();
      List<int> imageData = byteData.buffer.asUint8List();

      index_image = "image";

      http.MultipartFile multipartFile = http.MultipartFile.fromBytes(
        index_image,
        imageData,
        filename: '${index_image}-${i.toString()}.jpg',
        contentType: MediaType("image", "jpg"),
      );

      // add file to multipart
      request.files.add(multipartFile);
    }
    Map<String, String> headers = {
      "Accept": "application/json",
      'Authorization': _token
    };

    request.headers.addAll(headers);
    request.fields.addAll({
      "moduleId": moduleId,
      "time": DateTime.now().toIso8601String(),
      "type": type,
      "message": message,
      "info": info,
      "score": score.toStringAsFixed(1),
    });

    var response = await request.send();
    final respStr = await http.Response.fromStream(response);
    return respStr.statusCode;
  }

  Future<dynamic> postReportCheck(String moduleId, String type, String message,
      String info, double score) async {
    // String url = 'https://power.elefante.co.id/api/getNodeDataSingle';
    final response = await http.post('${url}/api/postReport', headers: {
      "Accept": "application/json",
      'Authorization': _token
    }, body: {
      "moduleId": moduleId,
      "time": DateTime.now().toIso8601String(),
      "type": type,
      "message": message,
      "info": info,
      "score": score.toStringAsFixed(1),
    });
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<List<dynamic>> getCheckupDevice(String moduleId) async {
    // String url = 'https://power.elefante.co.id/api/getCheckupDevice';
    final response = await http.post('${url}/api/getCheckupDevice',
        headers: {"Accept": "application/json", 'Authorization': _token},
        body: {"moduleId": moduleId});
    final responseData = json.decode(response.body);
    return responseData;
  }

  Future<void> signup(String email, String password, String username) async {
    // const url = 'https://power.elefante.co.id/api/signup';
    try {
      final response = await http.post(
        '${url}/api/signup',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(
          {"email": email, "password": password, "username": username},
        ),
      );

      final responseData = json.decode(response.body);
    } catch (error) {
      throw error;
    }
  }

  Future<void> login(String email, String password) async {
    // const url = 'https://power.elefante.co.id/api/login';
    try {
      final response = await http.post(
        '${url}/api/login',
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: json.encode(
          {"email": email, "password": password},
        ),
      );
      final responseData = json.decode(response.body);

      _token = responseData['token'];
      _userId = responseData['userId'];
      _expiryDate =
          DateTime.fromMillisecondsSinceEpoch(responseData['expired'] * 1000);

      // _autoLogout();
      notifyListeners();

      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
        {
          "token": _token,
          "userId": _userId,
          "expiryDate": _expiryDate.toIso8601String(),
        },
      );
      prefs.setString("userData", userData);
    } catch (error) {
      throw error;
    }
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("userData")) {
      return false;
    }
    final extractedUserData =
        json.decode(prefs.getString("userData")) as Map<String, Object>;
    final expiryDate = DateTime.parse(extractedUserData['expiryDate']);

    if (expiryDate.isBefore(DateTime.now())) {
      return false;
    }
    _token = extractedUserData["token"];
    _userId = extractedUserData["userId"];
    _expiryDate = expiryDate;

    notifyListeners();
    _autoLogout();
    return true;
  }

  void logout() async {
    _token = null;
    _userId = null;
    _expiryDate = null;
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }

    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey("userData")) {
    } else {
      prefs.remove("userData");
    }

    notifyListeners();
  }

  void _autoLogout() {
    if (_authTimer != null) {
      _authTimer.cancel();
    }
    final timeToExpiry = _expiryDate.difference(DateTime.now()).inSeconds;
    _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
