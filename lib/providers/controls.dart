import 'package:flutter/material.dart';
import "../models/control.dart";

class Controls with ChangeNotifier {
  List<Control> _items = [
    Control(
        id: "1",
        name: "control 1",
        ip: "iot.elefante.co.id",
        port: "183",
        topic: "control1",
        value: 1,
        type: ControlType.onof,
        min: 0,
        max: 1,
        step: 1),
    Control(
      id: "2",
      name: "control 2",
      ip: "iot.elefante.co.id",
      port: "183",
      topic: "control2",
      type: ControlType.slider,
      max: 100,
      min: 0,
      step: 1,
      value: 50,
    ),
    Control(
      id: "3",
      name: "control 3",
      ip: "iot.elefante.co.id",
      port: "183",
      topic: "control3",
      type: ControlType.value,
      min: 0,
      max: 100,
      value: 1,
      step: 1,
    ),
  ];
  List<Control> get items {
    return [..._items];
  }

  void addControl(Control control) {
    final newControl = Control(
        id: DateTime.now().toString(),
        name: control.name,
        ip: control.ip,
        port: control.port,
        topic: control.topic,
        value: control.value,
        max: control.max,
        min: control.min,
        step: control.step,
        type: control.type);
    _items.add(newControl);
    notifyListeners();
  }

  void updateProduct(String id, Control newcontrol) {
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      _items[prodIndex] = newcontrol;
      notifyListeners();
    } else {
      // print('...');
    }
  }

  void deleteProduct(String id) {
    _items.removeWhere((cont) => cont.id == id);
    notifyListeners();
  }

  Control findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }
}
